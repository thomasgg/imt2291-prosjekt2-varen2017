<?php
  require_once '../include/db.php';
  require_once '../classes/classUser.php';
  require_once '../classes/classStudent.php';
  require_once '../classes/classTeacher.php';
  require_once '../classes/classAdmin.php';
  require_once '../classes/classVideoDownload.php';
  require_once '../classes/classPlaylistDownload.php';
  require_once '../classes/classFileUpload.php';
  $user = new User($database);                                                                //Required on each new page; must instantiate before videoBrowse/Display
  require_once '../include/videoBrowse.php';
  require_once '../include/videoDisplay.php';

  
  if($user->isTeacher()) {
    $user = new Teacher($database);
  } else if($user->isAdmin()) {
    $user = new Admin($database);
  } else if($user->isStudent()) {
    $user = new Student($database);
  }

  
  if($_SERVER['REQUEST_METHOD'] == 'GET' ) {


 
    if(isset($_GET['browsePublic']) && $_GET['browsePublic'] == 'true' ) {                    //videoBrowse.php - Public video browse
      $globalUserPlaylists = array();                                                         //Global personal playlist array. Only playlists created by the user.
      $user->getVideoPlaylists($globalUserPlaylists);                                         //Function fills the array by reference.
      $numPlaylistElements = count($globalUserPlaylists);

      $globalPublicVideos = array();                                                          //Public video array. If user is logged in, user's videos will not be found here
      $user->getVideoFiles($globalPublicVideos, "public");                                    //Function fills the array by reference.
      $numPublicVideos = count($globalPublicVideos);
      globalVideoBrowse("public", $globalPublicVideos,
                        $globalUserPlaylists, $user->isLoggedIn(),
                        $numPublicVideos, $numPlaylistElements);
    }

    if(isset($_GET['browsePersonal']) && $_GET['browsePersonal'] == 'true') {                 //videoBrowse.php - Personal video browse
      if($user->isLoggedIn()) {
        $globalUserPlaylists = array();                                                       //Global personal playlist array. Only playlists created by the user.
        $user->getVideoPlaylists($globalUserPlaylists);                                       //Function fills the array by reference.
        $numPlaylistElements = count($globalUserPlaylists);

        $globalPersonalVideos = array();                                                      //Personal video array. Only videos uploaded by the user.
        $user->getVideoFiles($globalPersonalVideos, "personal");                              //Functions fills the array by reference.
        $numPersonalVideos = count($globalPersonalVideos);
        globalVideoBrowse("personal", $globalPersonalVideos,
                          $globalUserPlaylists, $user->isLoggedIn(),
                          $numPersonalVideos, $numPlaylistElements);
      }
    }

    if(isset($_GET['navSearch']) && strlen($_GET['navSearch']) == "true") { 					        //videoBrowse.php - Search result video browse
      
      $globalUserPlaylists = array();                                                         //Global personal playlist array. Only playlists created by the user.
      $user->getVideoPlaylists($globalUserPlaylists);                                         //Function fills the array by reference.
      $numPlaylistElements = count($globalUserPlaylists);

      $globalSearchResults = array();
      $user->getVideoFiles($globalSearchResults, "search", $_GET['navSearch']);
      $numSearchResults = count($globalSearchResults);

      globalVideoBrowse("search", $globalSearchResults, $globalUserPlaylists,
                        $user->isLoggedIn(), $numSearchResults, $numPlaylistElements);
    }

    if(isset($_GET['browseTracksRadioList']) && $_GET['browseTracksRadioList'] == "true") {   //videoTracksUpload.php - Radio button list
        $globalPersonalVideos = array();                                                      //Personal video array. Only videos uploaded by the user.
        $user->getVideoFiles($globalPersonalVideos, "personal");                              //Functions fills the array by reference.
        $numPersonalVideos = count($globalPersonalVideos);

        for($ii = 1; $ii <= $numPersonalVideos; $ii++) {                                      //For all personal videos, create a radio button.
          echo '<div class="radio">
                 <label>
                   <input type="radio" name="tracksVidId" value="'.$globalPersonalVideos[$ii]->getVideoId().'">'.$globalPersonalVideos[$ii]->getVideoTitle();
          echo   '</label>
               </div>';
        }
    }

    if(isset($_GET['videoPlaylist']) && $_GET['videoPlaylist'] == "true") {                   //playlistManage.php - Playlists with delete and move buttons
      $globalUserPlaylists = array();                                                         //Global personal playlist array. Only playlists created by the user.
      $user->getVideoPlaylists($globalUserPlaylists);                                         //Function fills the array by reference.
      $numPlaylistElements = count($globalUserPlaylists);                                     
                                                                                              
      if($numPlaylistElements == 0) {                                                         
        echo '<p>No playlists found</p>';                                                     
      } else {                                                                                
        for($ii = 1; $ii <= $numPlaylistElements; $ii++) {                                    //First for-loop loops through each playlist
          $globalUserPlaylists[$ii]->sortPlistByPosition();
          $numPlaylistVideos = $globalUserPlaylists[$ii]->getPlistNumVideos();
          echo '<li class="list-group-item">' .
                 '<div class="row">' .
                   '<div class="col-sm-2">' .
                     '<button type="button" title="Delete playlist"
                              class="playlistDeleteBtn btn btn-danger btn-sm"
                              data-playlistId="'.$globalUserPlaylists[$ii]->getPlistId().'">' .
                       '<span class="glyphicon glyphicon-trash"></span>' .
                     '</button>' .
                   '</div>' .
                   '<div class="col-sm-10">' .
                     '<h4 class="playlistTitle">' . $globalUserPlaylists[$ii]->getPlistName() . '</h4>' .
                   '</div>' .
                 '</div>' .
               '</li>' .

               '<li class="list-group-item">' .
                 '<ul class="list-group">';

          for($jj = 1; $jj <= $numPlaylistVideos; $jj++) {                                    //Second for-loop loops through each video in current playlist
            echo '<li class="list-group-item" id="playlistVideoNum' . $ii . '-' . $jj . '">' .
                   '<button type="button" title="Remove from playlist"
                            class="playlistvideoDeleteBtn btn btn-danger btn-sm pull-right"
                            data-plidVidId="'.$globalUserPlaylists[$ii]->getPlistVideoId()[$jj].'">' .
                     '<span class="glyphicon glyphicon-trash"></span>' .
                   '</button>' .

                   '<button id="moveDown' . $ii . '-' . $jj . '" type="button" title="Move down" class="moveDown btn btn-info btn-sm pull-right">' .
                     '<span class="glyphicon glyphicon-arrow-down"></span>' .
                   '</button>' .

                   '<button id="moveUp' . $ii . '-' . $jj . '" type="button" title="Move up" class="moveUp btn btn-info btn-sm pull-right">' .
                     '<span class="glyphicon glyphicon-arrow-up"></span>' .
                   '</button>' .

                   $globalUserPlaylists[$ii]->getPlistVideoTitle()[$jj] . '<br />' .
                   '<img class="img-thumbnail" src="uploads/images/'.$globalUserPlaylists[$ii]->getPlistVideoThumbnail()[$jj].'" height="120px" width="180px"></img>' .
                   '<input type="hidden" name="videoList[]" value="' . $ii . '-' . $jj . '-' . $globalUserPlaylists[$ii]->getPlistId() . '" class="playlistNum'.$ii.'">' .
                 '</li>';
          }
          echo   '</ul>' .
               '</li>';
        }
      }
    }

    if(isset($_GET['currentVid']) && strlen($_GET['currentVid']) > 0) {                       //videoDisplay.php - A selected video is displayed.
      $fileSub = "../uploads/transcripts/" . substr($_GET['currentVid'], 0, -4) . "_subs_en.vtt";   //WARNING: Filepath must be changed dependant on where its called from
      $fileChap = "../uploads/transcripts/" . substr($_GET['currentVid'], 0, -4) . "_chaps_en.vtt"; //WARNING: Filepath must be changed dependant on where its called from
      
      displaySelectedVideo($_GET['currentVid'], 
                           $user->isFileAvailable($fileSub),
                           $user->isFileAvailable($fileChap));
    }
    
    if(isset($_GET['watchRadioList']) && $_GET['watchRadioList'] == "true") {                 //videoWatchlist.php - Radio button list with personal videos.
      $globalPersonalVideos = array();                                                        //Personal video array. Only videos uploaded by the user.
      $user->getVideoFiles($globalPersonalVideos, "personal");                                //Functions fills the array by reference.
      $numPersonalVideos = count($globalPersonalVideos);                                      
                                                                                              
      for($ii = 1; $ii <= $numPersonalVideos; $ii++) {                                        //For all personal videos, create a radio button.
        echo '<div class="radio">
               <label>
                 <input type="radio" name="watchlistVidId" value="'.$globalPersonalVideos[$ii]->getVideoId().'">'.$globalPersonalVideos[$ii]->getVideoTitle();
        echo   '</label>
             </div>';
      }
    }
    
    if(isset($_GET['watchlistVidId']) && $_GET['watchlistVidId'] > 0) {                       //videoWatchlist.php - Watchfile contents of selected radio button.
      $user->getWatchfileContents($_GET['watchlistVidId']);
    }
   
    if(isset($_GET['userAccessLevel']) && $_GET['userAccessLevel'] == "true") {               //globalScripts.js - Sets scripts user access level
      if($user->isAdmin()) {
        echo "Admin";
      } else if($user->isTeacher()) {
        echo "Teacher";
      } else if($user->isStudent()) {
        echo "Student";
      }
    }
    
    if(isset($_GET['trackSubContents']) && strlen($_GET['trackSubContents']) > 0) {           //globalScripts.js - Gets the contents of a tracks subtitle file.
      echo file_get_contents("../" . $_GET['trackSubContents']);                              //WARNING: Filepath must be changed relative to where this file is called from.
    } 
    
    if(isset($_GET['trackChapContents']) && strlen($_GET['trackChapContents']) > 0) {         //globalScripts.js - Gets the contents of a tracks chapter file.
      echo file_get_contents("../" . $_GET['trackChapContents']);                             //WARNING: Filepath must be changed relative to where this file is called from.
    } 
    
    if(isset($_GET['isSubAvailable']) && strlen($_GET['isSubAvailable']) > 0) {               //globalScripts.js - Returns true/false if subs are available
      $fileSub = "../uploads/transcripts/" . substr($_GET['isSubAvailable'], 0, -4) . "_subs_en.vtt";   //WARNING: Filepath must be changed dependant on where its called from
      echo ($user->isFileAvailable($fileSub) ? "true" : "false");
    }
    
    if(isset($_GET['isChapAvailable']) && strlen($_GET['isChapAvailable']) > 0) {             //globalScripts.js - Returns true/false if chaps are available
      $fileChap = "../uploads/transcripts/" . substr($_GET['isChapAvailable'], 0, -4) . "_chaps_en.vtt"; //WARNING: Filepath must be changed dependant on where its called from
      echo ($user->isFileAvailable($fileChap) ? "true" : "false");
    }
    
    
    if(isset($_GET['navSearch']) && strlen($_GET['navSearch']) > 0) {                         //globalTopNav.php - Search results are loaded into video browse section.
      $globalSearchResults = array();
      $user->getVideoFiles($globalSearchResults, "search", $_GET['navSearch']);
      $numSearchResults = count($globalSearchResults);
      
      $globalUserPlaylists = array();                                                         //Global personal playlist array. Only playlists created by the user.
      $user->getVideoPlaylists($globalUserPlaylists);                                         //Function fills the array by reference.
      $numPlaylistElements = count($globalUserPlaylists);
      
     
      globalVideoBrowse("search", $globalSearchResults,
                        $globalUserPlaylists, $user->isLoggedIn(),
                        $numSearchResults, $numPlaylistElements);
    }
    
    if(isset($_GET['signout']) && $_GET['signout'] == 'true') {                               //globalTopNav.php - Signs the user out.
      $user->logOut();
    }
    

  } else if($_SERVER['REQUEST_METHOD'] == 'POST' ) {
   

    if(isset($_FILES['tracksFile']) && isset($_POST['tracksVidId'])) {                        //videoTracksUpload.php - Upload tracks file and add it to specified video
      $user->uploadUserFile(array($_FILES['tracksFile']));
      $user->addTracksToVideo($_POST['tracksVidId']);
    }

    if(isset($_POST['newUserPlaylist']) && strlen($_POST['newUserPlaylist']) > 0) {           //playlistManage.php - Save and upload new playlist
      $user->getPlaylistInfo($_POST['newUserPlaylist'], "new");                               //Saving the playlist to the user object
      $user->uploadVideoPlaylists("new");                                                     //Uploads the saved playlist
    }

    if(isset($_POST['newList']) && count($_POST['newList']) > 1) {                            //playlistManage.php - Save and upload updated playlist order
      $user->getPlaylistInfo($_POST['newList'], "update");
      $user->uploadVideoPlaylists("update");
    }

    if(isset($_POST['deletePlaylist']) && strlen($_POST['deletePlaylist']) > 0) {             //playlistManage.php - Delete playlist
      $user->deleteUserPlaylist($_POST['deletePlaylist']);
    }

    if(isset($_POST['deletePlaylistVideo']) && strlen($_POST['deletePlaylistVideo']) > 0) {   //playlistManage.php - Delete playlistvideo
      $user->deleteVideoFromPlaylist($_POST['deletePlaylistVideo']);
    }

    if(isset($_FILES['videoFile'])) {                                                         //videoUpload.php - Uploads video and video specific user data
      $user->uploadUserFile(array($_FILES['videoFile'], $_POST['videoThumbnail']));
      $user->uploadUserFileData(array('title' => $_POST['vidTitle'],
                                      'category' => $_POST['vidCategory'],
                                      'description' => $_POST['vidDescription'],
                                      'tags' => $_POST['vidTags']));
    }

    if(isset($_POST['vidToPlist']) && strlen($_POST['vidToPlist']) > 0) {                     //videoBrowse.php - Adds a public/personal video to the user's playlist
      $user->addVideoToPlaylist($_POST['vidToPlist']);
    }

    if(isset($_POST['deleteVideoId']) && strlen($_POST['deleteVideoId']) > 0) {               //videoBrowse.php - Deletes a personal video.
      $user->deleteUserVideo($_POST['deleteVideoId']);
    }

    if(isset($_POST['watchStartTime']) && isset($_POST['watchEndTime'])) {                    //videoWatchlist.php - Adds a student watch entry to watchfile
      $user->registerWatchTime($_POST['watchStartTime'], $_POST['watchEndTime'], 
                               $_POST['watchVidId']);
    }
    
    if(isset($_POST['resetWatchfile']) && strlen($_POST['resetWatchfile']) > 0) {             //videoWatchlist.php - Clears a watchlist empty
      $user->resetWatchfileContents($_POST['resetWatchfile']);
    }
    
    if(isset($_POST['signInEmail']) && isset($_POST['signInPassword'])) {                     //userSignin.php - Signs in a user if email/pw is accepted.
      $user->logIn($_POST['signInEmail'], $_POST['signInPassword']);
      if(isset($_POST['rememberMe']) && $_POST['rememberMe'] == true) {
        $user->rememberMe();
      }
    }
    
    if(isset($_POST['registerEmail']) && isset($_POST['registerPassword'])) {                 //userRegister.php - Registers a new user.
      $user->registerUser($_POST['registerFname'], $_POST['registerLname'],
                          $_POST['registerEmail'], $_POST['registerPassword']);
    }
    
    if(isset($_POST['userDelete']) && strlen($_POST['userDelete']) > 0) {                     //userDelete.php - Deletes an existing user.
      $user->deleteUser($_POST['userDelete']);
    }
    
    if(isset($_POST['tracksContent']) && isset($_POST['tracksFilePath'])) {                   //globalScripts.php - Writes and saves tracks file live-input from user.
      $fp = "../" . $_POST['tracksFilePath'];
      $user->updateLiveTracksFile($_POST['tracksContent'], $fp);
    }
  }
?>
