<div class="panel panel-default">
  <div class="panel-heading">
    <h5 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseRegisterUser">Add new user (admin)</a>
    </h5>
  </div>
  <div id="collapseRegisterUser" class="panel-collapse collapse">
    <div class="panel-body">
      <form id="formUserRegister" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
        <label for="firstName">First name:</label>
        <input class="form-control" type="text" name="firstName" placeholder="First name" required>
        <br>
        <label for="lastName">Last name:</label>
        <input class="form-control" type="text" name="lastName" placeholder="Surname" required>
        <br>
        <label for="email">E-mail:</label>
        <input class="form-control" type="text" name="email" placeholder="Email address" required>
        <br>
        <label for="password">Password:</label>
        <input class="form-control" type="password" name="password" placeholder="Password" required>
        <br>
        <button class="btn btn-primary" type="submit" name="submit" value="Register">Register</button>
      </form>
    </div>
  </div>
</div>