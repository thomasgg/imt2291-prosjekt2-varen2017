<div id="globalTopNav">
  <nav class="navbar navbar-default navbar-fixed-top">
  
    <div class="navbar-header">
      <!-- start collapse button -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topNav">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- end collapse button -->
      <a class="navbar-brand" href="main.php">LearnItRight</a>
    </div>
      
    <!-- start collapsable right-side elements -->
    <div class="collapse navbar-collapse" id="topNav">
      <ul class="nav navbar-nav navbar-right">
        <?php 
          if($user->isLoggedIn()) {
        ?> 
            <li>
              <a href="">
                <span class="glyphicon glyphicon-user"></span> Inside (<?php echo $user->getUsername(); ?>)
              </a>
            </li> 
            <li>
              <a id="userSignOut" href="">
                <span class="glyphicon glyphicon-log-out"></span> Sign out
              </a>
            </li>
        <?php
          } else if(!($user->isLoggedIn())) {
        ?>
            <li>
              <a data-toggle="collapse" href="#collapseLoginUser">
                <span class="glyphicon glyphicon-log-in"></span> Sign in
              </a>
            </li>
        <?php 
          }
        ?>
      </ul>
      
      <!-- start collapsable left-side elements -->
      <ul class="nav navbar-nav navbar-left">
        <!-- start search box -->
        <form id="formGlobalTopNav" method="GET" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" class="pull-left form-inline">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search videos" name="navSearch">
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </div>
          </div>
        </form>
      </ul>
      <!-- end search box -->
    </div>
    <!-- end collapsable right-side links -->
    
  </nav>
</div>