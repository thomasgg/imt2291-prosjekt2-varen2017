<?php 
  function globalVideoBrowse($category, &$videoDisplayList, &$playlistDisplayList,  
                             $isLoggedIn, $videoNumElements, $playlistNumElements=0) {
    $endFlag = false;                                                          //Using endFlag to handle "less than 3 vids in an end-row" -situations
    
    if($category == "search") {                                                //If searching; display proper text
      echo '<div id="videoDisplay">' .
             '<h5 class="videoBrowse">Browse search results<h5>' .
             '<br>';       
    } else {                                                                   //If browsing; display proper text
      echo '<div id="videoDisplay">' .
             '<h5 class="videoBrowse">Browse '.$category.' videos<h5>' .
             '<br>';      
    }

    if(count($videoDisplayList) > 0) {
      echo   '<div class="row">';
      for($ii = 1; $ii <= $videoNumElements; $ii++) {
        $endFlag = false;                                
        echo '<div class="col-md-4">';
             
        echo   '<div class="row">' .                                           //Display video thumbnail image. Link sends video ID with HTTP GET.
                 '<div class="col-md-12">' .
                   '<a class="videoDisplayUrl" href="'.$videoDisplayList[$ii]->getVideoFileName().'">' .
                     '<img class="img-thumbnail" src="uploads/images/'.$videoDisplayList[$ii]->getVideoThumbnail().'"></img>' .
                   '</a>' .
                 '</div>' .
               '</div>';
        
         echo  '<div class="row row-top-margin">';                 
                 if($isLoggedIn && $category == "personal") {                  //If logged in and browse personal videos; Display video title, playlist button and delete button.
         echo      '<div class="col-md-7">' .
                     '<a class="videoDisplayUrl" href="'.$videoDisplayList[$ii]->getVideoFileName().'">' .
                       $videoDisplayList[$ii]->getVideoTitle() .
                     '</a>' .
                   '</div>' .
                   '<div class="col-md-2">' .
                     '<div class="dropdown addPlistBtn">' .                    //Playlist dropdown button in public/personal/search browse lists.
                       '<button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">' .        
                         'Add &nbsp;' .
                         '<span class="glyphicon glyphicon-plus-sign"></span>' .
                       '</button>' .
                       '<ul class="dropdown-menu">';
                         for($jj = 1; $jj <= $playlistNumElements; $jj++) {
         echo              '<li class="dropdown">' .
                             '<a id="'.$playlistDisplayList[$jj]->getPlistId().'-'.$videoDisplayList[$ii]->getVideoId().'" 
                                 href="#" class="addToPlistBrowseDropdown">' .
                               $playlistDisplayList[$jj]->getPlistName() .
                             '</a>' .
                           '</li>';
                         }
         echo          '</ul>' .
                     '</div>' .
                   '</div>' .
                   '<div class="col-md-1">' .                                  //Delete video button. Only shown in personal browse list.
                     '<button type="button" class="deletePersonalVidBtn btn btn-danger btn-xs" 
                              data-videoId="'.$videoDisplayList[$ii]->getVideoId().'">' .
                       '<span class="glyphicon glyphicon-trash"> </span>' .
                     '</button>' .
                   '</div>' .
                   '<div class="col-md-2"></div>';
                       
                  
                 } else if(($isLoggedIn && $category == "public") || ($isLoggedIn && $category == "search")) {         //If logged in and browse public videos or logged in and searching; Display video title and playlist button
        echo      '<div class="col-md-8">' .
                     '<a class="videoDisplayUrl" href="'.$videoDisplayList[$ii]->getVideoFileName().'">' .
                      $videoDisplayList[$ii]->getVideoTitle() .
                     '</a>' .
                   '</div>' .
                   '<div class="col-md-2">' .
                     '<div class="dropdown addPlistBtn">' .                    //Playlist dropdown button in public/personal/search browse lists.
                       '<button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown">' .        
                         'Add &nbsp;' .
                         '<span class="glyphicon glyphicon-plus-sign"></span>' .
                       '</button>' .
                       '<ul class="dropdown-menu">';
                         for($jj = 1; $jj <= $playlistNumElements; $jj++) {
         echo              '<li class="dropdown">' .
                             '<a id="'.$playlistDisplayList[$jj]->getPlistId().'-'.$videoDisplayList[$ii]->getVideoId().'" 
                                 href="#" class="addToPlistBrowseDropdown">' .
                               $playlistDisplayList[$jj]->getPlistName() .
                             '</a>' .
                           '</li>';
                         }
         echo          '</ul>' .
                     '</div>' .
                   '</div>' .
                   '<div class="col-md-2"></div>';
                   
                 
                 } else {                                                     //Video tittle is always shown
         echo     '<div class="col-md-12">' .
                    '<a class="videoDisplayUrl" href="'.$videoDisplayList[$ii]->getVideoFileName().'">' .
                     $videoDisplayList[$ii]->getVideoTitle() .
                    '</a>' .
                  '</div>';
                 }
         echo  '</div>';
                   
         echo    '<div class="row row-top-margin">' .                          //Video uploader name is always shown
                   '<div class="col-md-12">' .
                     '<small>' . $videoDisplayList[$ii]->getVideoUploaderName() . '</small>' .
                   '</div>' .
                 '</div>';
                    
         echo    '<div class="row row-bottom-margin">' .                       //Video duration is always shown 
                   '<div class="col-md-12">' .
                     '<small>' . $videoDisplayList[$ii]->getVideoDuration() . '</small>' .
                   '</div>' .
                 '</div>';
                  
              
         echo  '</div>';                                                       //top div col-md-4 end
        if($ii%3 == 0) {                                                       //For every third video..
          echo '</div>' .                                                      //..end the row..
               '<hr>' .                                                        //..create a line..
               '<div class="row top-buffer">';                                 //..create a new row.
          $endFlag = true;                                                     //This row has 3 videos
       }                                                                       
      }                                                                        
      if($endFlag == false) {                                                  //If final row did not have 3 videos
        echo '</div>' .                                                        //..end the row..
             '<hr>';                                                           //..create a line..
      }                                                                        
    } else {                                                                   
      echo 'No videos found...';                                               //If video array is empty
    }                                                                          
    echo '</div>';                                                             //div videoDisplay end
  }
?>