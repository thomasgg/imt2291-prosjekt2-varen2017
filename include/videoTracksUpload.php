  <div class="panel panel-default">
    <div class="panel-heading">
      <h5 class="panel-title">
        <a id="tracksUploadSidepanel" data-toggle="collapse" data-parent="#accordion" href="#collapseVideoTracksUpload">Add subtitles & chapters</a>
      </h5>
    </div>
    <div id="collapseVideoTracksUpload" class="panel-collapse collapse">
      <div class="panel-body">
        <form id="formTracksUpload" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
          <div id="tracksRadioList">
            <!-- The list of radio buttons for each personal video -->
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <input type="file" name="tracksFile" id="tracksFile">
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4">
              <button id="tracksUploadBtn" class="btn btn-primary" type="submit" value="Upload video">Upload</button>
            </div>
          </div>
          <div class="jumbotron">
          <h4>Choose a personal video, add tracks (.vtt) file. <br><br>
              - If subtitles; the cue IDs <strong>must</strong> contain "subtitle".<br><br>
              - If chapters; the cue IDs <strong>must</strong> contain "chapter".
          </h4>
          </div>
        </form>
      </div>
    </div>
  </div>