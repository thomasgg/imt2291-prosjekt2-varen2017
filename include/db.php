<?php
$servername = "127.0.0.1";
$adminuser = "thomas";
$adminpassword = "thomas";
$dbname = "oppgaver";
try {
    $database = new PDO("mysql:host=$servername;dbname=$dbname", $adminuser, $adminpassword);
    $database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $err) {
    if(isset($deug)) {
      echo "Connection to database failed: " . $err->getMessage();
    } else {
      echo "Oops! Something went wrong, please try again later.";
    }
}

?>
