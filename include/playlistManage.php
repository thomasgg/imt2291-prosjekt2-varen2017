<!-- 
   THE VIDEO SORTING WORKS LIKE THIS: 
   Sends an array with each video in every playlist belonging to the user.
   Each array index value has three digits separated by hyphens (-).
   The array index number is the updated (or untouched) video position in the playlist.
   The first index value digit is the user's local playlist number.
   The second index value digit is the video position in the playlist before the user performed changes.
   The third index value digit is the unique video playlist ID registered in the database.
   Example array with 5 videos from index 1 to 5
      [1] 1-2-15 | Local playlist: 1, database playlist: 15, current playlist position: 1, previous playlist position: 2
      [2] 1-1-15 | Local playlist: 1, database playlist: 15, current playlist position: 2, previous playlist position: 1
      [3] 1-3-15 | Local playlist: 1, database playlist: 15, current playlist position: 3, previous playlist position: 3 (unmoved)
      [4] 1-4-15 | Local playlist: 1, database playlist: 15, current playlist position: 4, previous playlist position: 4 (unmoved)
      [5] 1-5-15 | Local playlist: 1, database playlist: 15, current playlist position: 5, previous playlist position: 5 (unmoved)
-->

<div class="panel panel-default">
  <div class="panel-heading">
    <h5 class="panel-title">
      <a id="playlistManageSidepanel" data-toggle="collapse" data-parent="#accordion" href="#collapsePlaylistManage">Manage playlists</a>
    </h5>
  </div>
  <div id="collapsePlaylistManage" class="panel-collapse collapse">
    <div id="playlistManagePanelBody" class="panel-body">   
      <form method="post" id="formPlaylistEdit" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
        <ul id="videoPlaylist" class="list-group list-unstyled">

        </ul>
        <input class="form-control" type="text" name="newUserPlaylist" placeholder="New playlist">
        <br>
        <button id="savePlaylistChangesBtn" type="submit" class="btn btn-success btn-md btn-block">
          <strong>Save changes</strong>
        </button>
      </form>
    </div> 
  </div>
</div>