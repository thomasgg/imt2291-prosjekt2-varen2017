<div class="panel panel-default">
  <div class="panel-heading">
    <h5 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseDeleteUser">Delete user (admin)</a>
    </h5>
  </div>
    <div id="collapseDeleteUser" class="panel-collapse collapse">
      <div class="panel-body">
        <form id="formUserDelete" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>">
          <input class="form-control" type="text" name="userDelete" placeholder="User E-mail">
          <br />
          <button class="btn btn-danger" type="submit" name="submit" value="Delete">Delete</button>
        </form>
      </div>
    </div>
</div>