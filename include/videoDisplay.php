<?php
function displaySelectedVideo($vid, $subsExists, $chapsExists) {
  if($subsExists) {
    $trackSubs = substr($vid, 0, -4) . "_subs_en.vtt";                                           
  }
  if($chapsExists) {
    $trackChaps = substr($vid, 0, -4) . "_chaps_en.vtt"; 
  }
  echo   '<div class="row">';
          if($chapsExists) {
  echo      '<div class="col-md-9">';                                          //If chapters exists, make space for chapters
          } else {
  echo      '<div class="col-md-12">';                                         //Else give all the space to the video player
          }
  echo       '<video id="videoCurrent" type="video/mp4" controls autoplay>
                <source src="uploads/videos/'.$vid.'">';
                if($chapsExists) {                                             
  echo            '<track id="trackChaps" kind="chapters" label="Chapters"
                         src="uploads/transcripts/'.$trackChaps.'"
                         srclang="en" default>
                  </track>';
                }
                if($subsExists) {
  echo           '<track id="trackSubs" kind="subtitles" label="English subtitles"
                         src="uploads/transcripts/'.$trackSubs.'"
                         srclang="en" default>
                  </track>';
                }
  echo       '</video>
            </div>';
            if($chapsExists) {
  echo      '<div class="col-md-3" >
               <div class="row">
                 <div class="col-md-9" id="displayVideoSubsRight">
                   <ul id="rightChapsView" class="list-unstyled"></ul>
                 </div>
                 <div class="col-md-3"></div>
               </div>
               <div class="row">
                 <div id="rightChapsEdit" class="col-md-12">
                   <textarea id="chapsTextArea" class="hideMe"> </textarea>
                 </div>
               </div>
             </div>';
            }
  echo   '</div>
          <div class="row">
            <div class="col-sm-9" id="displayVideoSubsBottom">
              <div id="bottomSubsView">
                &nbsp;
              </div>                         
              <div id="bottomSubsEdit" class="hideMe">
                <textarea id="subsTextArea"> </textarea>
              </div>
            </div>
            <div class="col-sm-3"></div>
          </div>
          <br><br>';
}
?>