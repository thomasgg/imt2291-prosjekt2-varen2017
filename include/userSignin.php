<div class="panel panel-default">
  <div class="panel-heading">
    <h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseLoginUser">Sign in</a>
    </h5>
  </div>
  <div id="collapseLoginUser" class="panel-collapse collapse">
    <div class="panel-body">
      <form id="formSignIn" method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" />
        <label for="email">Email:</label>
        <input id="signInEmail" class="form-control" type="text" name="email" placeholder="Email address">
        <br>
        <label for="password">Password:</label>
        <input id="signInPassword" class="form-control" type="password" name="password" placeholder="Password">
        <br>
        <button class="btn btn-primary" type="submit">Sign in</button>
        <div class="input-group">
          <div class="checkbox">
            <label><input type="checkbox" name="rememberMe" id="rememberMeChk" >Remember me</label>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>