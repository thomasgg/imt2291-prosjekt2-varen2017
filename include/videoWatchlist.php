<div class="panel panel-default">
  <div class="panel-heading">
    <h5 class="panel-title">
      <a id="videoWatchlistSidepanel" data-toggle="collapse" data-parent="#accordion" href="#collapseVideoWatchlist">Video watchlist</a>
    </h5>
  </div>
  <div id="collapseVideoWatchlist" class="panel-collapse collapse">
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <div id="watchRadioList">
            <!-- The list of radio buttons for each personal video -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="jumbotron">
            <div id="watchlistContents">
              <!-- The contents of the selected radio button -->
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <button id="resetWatchfileBtn" class="btn btn-danger" type="button">Reset watchfile</button>
        </div>
      </div>
    </div>
  </div>
</div>