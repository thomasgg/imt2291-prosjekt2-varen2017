  <div class="panel panel-default">
    <div class="panel-heading">
      <h5 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapseVideoUpload">Upload video</a>
      </h5>
    </div>
    <div id="collapseVideoUpload" class="panel-collapse collapse">
      <div class="panel-body">
        <form id="formVideoUpload" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post" enctype="multipart/form-data">
          <label for="vidTitle">Title</label>
          <input class="form-control" type="text" name="vidTitle" maxlength="40" placeholder="Unique video title" required/>
          <br />
          
          <div class="form-group"> 
            <label for="vidCategory">Select category</label>
            <select class="form-control" name="vidCategory" id="vidCategory">
              <option>Maths</option>
              <option>Computing</option>
              <option>Science/engineering</option>
              <option>Economics/finance</option>
              <option selected="selected">Other</option>
            </select>
          </div>
          
          <div class="form-group">
            <label for="vidDescription">Description</label>
            <textarea class="form-control" placeholder="Description" name="vidDescription" id="vidDescription" rows="3" maxlength="100"> </textarea>
          </div>
          
          <label for="vidTags">Keywords/tags</label>
          <input class="form-control" type="text" name="vidTags" id="vidTags" placeholder="Keywords separated by comma" required/>
          <br />
         
          <br>
        <div class="row">
          <div class="col-md-6">
            <input type="file" name="videoFile" id="videoFile" required>
            <input type="hidden" name="videoThumbnail" id="videoThumbnail">
          </div>
          <div class="col-md-2"></div>
          <div class="col-md-4">
            <button id="videoUploadBtn" class="btn btn-primary" type="submit" value="Upload video">Upload</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>