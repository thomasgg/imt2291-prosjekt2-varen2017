  <?php 
  require_once 'include/db.php';
  require_once 'classes/classUser.php';  
  $user = new User($database);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/main.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="scripts/js/globalScripts.js"></script>
  <script src="scripts/js/globalScriptFunk.js"></script>
</head>
<body>
  <div id="main">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <?php require_once 'include/globalTopNav.php'; ?> 
        </div>
      </div>
      <div id="row">                                                         <!-- Main website row -->
       <div class="col-md-1"> </div>
        <div class="col-md-3">                                               <!-- Left side panel column -->
          <div class="row">
            <div class="col-md-6">                                           <!-- Public video display button -->
              <button type="button" class="btn btn-info btn-sm btn-block"    
                      id="browsePublicBtn">Public</button> 
            </div>
            <div class="col-md-6">                                           <!-- Personal video display button-->
              <button type="button" class="btn btn-info btn-sm btn-block"    
                      id="browsePersonalBtn">Personal</button> 
            </div>
          </div>
          <div class="panel-group" id="accordion">                           <!-- Collapsable panels -->
            <?php 
               if(!$user->isLoggedIn()) {
                 require_once 'include/userSignin.php';
                   
               } else if($user->isStudent()) {
                 require_once 'include/playlistManage.php';
                 
               } else if($user->isTeacher()) {  
                require_once 'include/videoUpload.php'; 
                require_once 'include/videoTracksUpload.php';
                require_once 'include/playlistManage.php';
                require_once 'include/videoWatchlist.php';
                
               } else if($user->isAdmin()) {
                require_once 'include/videoUpload.php'; 
                require_once 'include/videoTracksUpload.php';
                require_once 'include/playlistManage.php';
                require_once 'include/videoWatchlist.php';
                require_once 'include/userRegister.php';
                require_once 'include/userDelete.php'; 
               }
            ?>
          </div>   
        </div>
        <div class="col-md-7">                                               <!-- Right side video browse-->
          <div class="row">
            <div class="col-md-12" id="videoDisplay">                        
              <!-- Display (view) a clicked video -->
            </div>
          </div>
          <div class="row">
            <div class="col-md-12" id="globalVideoBrowse">
              <!-- Browse "search" / "public" / "personal" videos -->
            </div>
          </div>                        
        </div>
        <div class="col-md-1"> </div>  
      </div>
    </div>
  </div>
 </body>
</html>
