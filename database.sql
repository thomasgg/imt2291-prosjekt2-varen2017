
-- USERS
CREATE TABLE IF NOT EXISTS `user` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `password` char(255) NOT NULL,
  `firstName` varchar(128) NOT NULL,
  `lastName` varchar(128) NOT NULL,
  `regDate` TIMESTAMP NOT NULL,
  `isAdmin` enum('y','n') NOT NULL DEFAULT 'n',
  `isTeacher` enum('y','n') NOT NULL DEFAULT 'n',
  `isStudent` enum('y','n') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB;


-- PERSISTENT LOGIN
CREATE TABLE IF NOT EXISTS `cookierm` (
  `cookieId` bigint(20) NOT NULL,
  `series` char(32) NOT NULL,
  `token` varchar(255) NOT NULL,
  `expiresDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cookieId`)
) ENGINE=InnoDB;


-- VIDEO
CREATE TABLE IF NOT EXISTS `video` (
  `videoId` bigint(20) NOT NULL AUTO_INCREMENT,
  `uploaderId` bigint(20) NOT NULL,            
  `title` varchar(128) NOT NULL,              
  `category` varchar(200) NOT NULL,          
  `tags` varchar(128) NOT NULL,               
  `description` text,                        
  `duration` varchar(16),                     
  `vidFileName` varchar(200) NOT NULL,
  `transFileName` varchar(200),
  PRIMARY KEY (`videoId`)
) ENGINE=InnoDB;


-- PLAYLIST
CREATE TABLE IF NOT EXISTS `playlist` (
  `playlistId` bigint(20) NOT NULL AUTO_INCREMENT,
  `uploaderId` bigint(20) NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`playlistId`)
) ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `playlistvideo` (
  `playlistVideoId` bigint(20) NOT NULL AUTO_INCREMENT,
  `playlistId` bigint(20) NOT NULL,
  `videoId` bigint(20) NOT NULL,
  `videoPos` bigint(20) NOT NULL,
  PRIMARY KEY (`playlistVideoId`)
) ENGINE=InnoDB;

-- ----------------------------------------------------------------------------------
-- FOREIGN KEYS
-- ----------------------------------------------------------------------------------

ALTER TABLE `video`
ADD CONSTRAINT FK_video
FOREIGN KEY (`uploaderId`) REFERENCES user(`userId`)
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `playlist`
ADD CONSTRAINT FK_playlist
FOREIGN KEY (`uploaderId`) REFERENCES user(`userId`)
ON UPDATE CASCADE
ON DELETE CASCADE;

ALTER TABLE `playlistvideo`
ADD CONSTRAINT FK_playlistvideo
FOREIGN KEY (`playlistId`) REFERENCES playlist(`playlistId`)
ON UPDATE CASCADE
ON DELETE CASCADE;

/* 
  First time log on users: email            password
                          --------------------------
                          "admin@admin"     - "passord"
                          "teacher@teacher" - "passord"
                          "student@student" - "passord"
*/
INSERT INTO user (email, password, firstName, lastName, isAdmin, isStudent, isTeacher)
VALUES ("admin@admin", "$2y$10$bO6XPlZLWpSPpHcdhC3WueegALWfnifAhRbgS9EyL0rZ6HXiDaGoq", "AdminBob", "Adminer", "y", "n", "n");

INSERT INTO user (email, password, firstName, lastName, isAdmin, isStudent, isTeacher)
VALUES ("teacher@teacher", "$2y$10$bO6XPlZLWpSPpHcdhC3WueegALWfnifAhRbgS9EyL0rZ6HXiDaGoq", "TeacherCarl", "Teacher", "n", "n", "y");

INSERT INTO user (email, password, firstName, lastName, isAdmin, isStudent, isTeacher)
VALUES ("student@student", "$2y$10$bO6XPlZLWpSPpHcdhC3WueegALWfnifAhRbgS9EyL0rZ6HXiDaGoq", "StudentEric", "Studenter", "n", "y", "n");

-- ----------------------------------------------------------------------------------
-- DATA FOR DEV TESTING -- DATA FOR DEV TESTING -- DATA FOR DEV TESTING -- DATA FOR DEV TESTING 
-- ----------------------------------------------------------------------------------
/*

INSERT INTO user (email, password, firstName, lastName)
VALUES ("test1@test", "test", "test1", "tester1");
INSERT INTO user (email, password, firstName, lastName)
VALUES ("test2@test", "test", "test2", "tester2");
INSERT INTO user (email, password, firstName, lastName)
VALUES ("test3@test", "test", "test3", "tester3");
INSERT INTO user (email, password, firstName, lastName)
VALUES ("test4@test", "test", "test4", "tester4");
INSERT INTO user (email, password, firstName, lastName)
VALUES ("test5@test", "test", "test5", "tester5");
---------------------------------------------------------

-- Uploader 1 ----
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (1, "title1", "subject1", "tags1", "description1", "duration1", "file1.txt", "trans1.txt");
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (1, "title2", "subject2", "tags2", "description2", "duration2", "file2.txt", "trans2.txt");
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (1, "title3", "subject3", "tags3", "description3", "duration3", "file3.txt", "trans3.txt");
-- Uploader 2 ---
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (2, "title4", "subject4", "tags4", "description4", "duration4", "file4.txt", "trans4.txt");
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (2, "title5", "subject5", "tags5", "description5", "duration5", "file5.txt", "trans5.txt");
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (2, "title6", "subject6", "tags6", "description6", "duration6", "file6.txt", "trans6.txt");
-- Uploader 3 ---
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (3, "title7", "subject7", "tags7", "description7", "duration7", "file7.txt", "trans7.txt");
-- Uploader 4 ---
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (4, "title8", "subject8", "tags8", "description8", "duration8", "file8.txt", "trans8.txt");
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (4, "title9", "subject9", "tags9", "description9", "duration9", "file9.txt", "trans9.txt");
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (4, "title10", "subject10", "tags10", "description10", "duration10", "file10.txt", "trans10.txt");
-- Uploader 5 --
INSERT INTO video (uploaderId, title, category, tags, description, duration, vidFileName, transFileName)
VALUES (5, "title11", "subject11", "tags11", "description11", "duration11", "file11.txt", "trans11.txt");


--------------------------------------------------------

-- Playlist uploaderId 1 start----
INSERT INTO playlist (uploaderId, name)
VALUES (1, "Testplaylist1");
INSERT INTO playlist (uploaderId, name)
VALUES (1, "Testplaylist2");
INSERT INTO playlist (uploaderId, name)
VALUES (1, "Testplaylist3");
-- Playlist uploaderId 2 start ----
INSERT INTO playlist (uploaderId, name)
VALUES (2, "Testplaylist4");
INSERT INTO playlist (uploaderId, name)
VALUES (2, "Testplaylist5");
-- Playlist uploaderId 3 start----
INSERT INTO playlist (uploaderId, name)
VALUES (3, "Testplaylist6");

----------------------------------------------------------


-- Uploader 1, playlist 1 start ----
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (1, 1, 1);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (1, 2, 2);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (1, 3, 3);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (1, 4, 4);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (1, 5, 5);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (1, 6, 6);
-- Uploader 1, playlist 2 start -----
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (2, 2, 1);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (2, 3, 2);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (2, 4, 3);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (2, 5, 4);
-- Uploader 1, pplaylist 3 start ----
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (3, 1, 1);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (3, 8, 2);
-- Uploader 2, playlist 1 start ---
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (4, 10, 1);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (4, 9, 2);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (4, 8, 3);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (4, 7, 4);
-- Uploader 2, plyalist 2 start ---
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (5, 7, 1);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (5, 8, 2);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (6, 1, 1);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (6, 2, 2);
INSERT INTO playlistvideo (playlistId, videoId, videoPos)
VALUES (6, 3, 3);

-------------------------------------------------------------------------

DELETE FROM video WHERE title LIKE "title%";
ALTER TABLE video AUTO_INCREMENT = 1;

DELETE FROM playlist WHERE name LIKE "testplaylist%";
ALTER TABLE playlist AUTO_INCREMENT = 1;

DELETE FROM playlistvideo;
ALTER TABLE playlistvideo AUTO_INCREMENT = 1;

DELETE FROM user WHERE password = "test";
ALTER TABLE user AUTO_INCREMENT = 2;

*/
