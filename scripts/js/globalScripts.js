var user = {                                                                   //A user namespace which lives outside the document.ready scope
             accessLevel: "",                                                  //Access levels are Student/Teacher/Admin
             isSubsEditBoxOpen: false,                                         
             isChapsEditBoxOpen: false,
             isSubAvailable: false,
             isChapAvailable: false,
           }
                                                       
$(document).ready(function() {
  getUserAccessLevel();                                                             //Categorizes the user as student/teacher/admin
  
  $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePublic=true");        //Public video browse on page load.
  $("#tracksRadioList").load("serverlogic/serverLogic.php?browseTracksRadioList=true"); //Tracks radio button list on page load.
  $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");           //Personal video playlist on page load.
  $("#watchRadioList").load("serverlogic/serverLogic.php?watchRadioList=true");         //Watchlist radio button list on page load.

  $("#formSignIn").submit(function(event) {                                         //userSignin.php - Signin form.
    var email = $("#signInEmail")[0].value;
    var pw = $("#signInPassword")[0].value;
    rm = false;                                                                     //Remember me is false unless if statement finds the checked box
    if($("#rememberMeChk:checked")[0]) {
      var rm = true;
    }
    $.post("serverlogic/serverLogic.php", 
    {
      signInEmail : email,
      signInPassword : pw,
      rememberMe : rm
      
    }, function() {
      location.reload();                                                            //Page refresh for login.
    });
    event.preventDefault();
  });
 
  $("#formUserRegister").submit(function(event) {                                   //userRegister.php - Register user form.
  var fName = $("#formUserRegister [name=firstName]")[0].value;
  var lName = $("#formUserRegister [name=lastName]")[0].value;
  var email = $("#formUserRegister [name=email]")[0].value;
  var pw    = $("#formUserRegister [name=password]")[0].value;
    $.post("serverlogic/serverLogic.php", 
      {
        registerEmail : email,
        registerPassword : pw,
        registerFname : fName,
        registerLname : lName
      }, function() {
        $("#formUserRegister")[0].reset();                                          //Resets the form input vaules
      });
      
    event.preventDefault();
  });
 
  $("#formUserDelete").submit(function(event) {                                     //userDelete.php - Delete user form.
    if(confirmDelete()) {
      var user = $("#formUserDelete [name=userDelete]")[0].value;
      $.post("serverlogic/serverLogic.php", { userDelete : user }, function() {
        $("#formUserDelete")[0].reset();
        alert("User account deleted");
      }); 
      event.preventDefault();
    }
  });
 
  $("#formGlobalTopNav").submit(function(event) {                                   //globalTopNav.php - Search box in top nav submit handler.
    var key = $("#formGlobalTopNav [name=navSearch]")[0].value;
    $("#globalVideoBrowse").load("serverlogic/serverLogic.php?navSearch=" + key);        //Public video browse on page load.
    event.preventDefault();
  });
 
  $("#browsePublicBtn").click(function() {                                          //videoBrowse.php - Public video browse on button click.  
    $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePublic=true");
  });
  
  $("#browsePersonalBtn").click(function() {                                        //videoBrowse.php - Personal video browse on button click.
    $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePersonal=true");
 });   
  
  $("#tracksUploadSidepanel").click(function() {                                    //videoBrowse.php - Personal video browse on button click.
    $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePersonal=true");
  });
  
  $("#globalVideoBrowse").on('click', 'a.addToPlistBrowseDropdown', function(e) {   //videoBrowse.php - Add to playlist dropdown list
    var id = this.id; 																															//Id consists of playlistID and videoID
    $.ajax({
      url: "serverlogic/serverLogic.php",
      type: "POST",
      data: { vidToPlist : id },
      error: function(xhr, status, error) {
        alert(error);
      },
      success: function(result) {
        $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");     //Personal video playlist updates automatically
      }
    });
    e.preventDefault();                                                             //Prevents href="#"
  });
  
  $("#globalVideoBrowse").on('click', 'button.deletePersonalVidBtn', function() {   //videoBrowse.php - Delete personal video button.
    if(confirmDelete()) {                                                           //If alert box is accepted  
      var vidId = $(this).attr("data-videoId");
      $.ajax({
        url: "serverlogic/serverLogic.php",
        type: "POST",
        data: { deleteVideoId : vidId },
        error: function(xhr, status, error) {
          alert(error);
        },
        success: function(result) {
          $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");      //Personal video playlist updates automatically
          $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePersonal=true"); //Personal video browse section updates automatically
        }
      });
    }
  });
    
  $("#globalVideoBrowse").on('click', 'a.videoDisplayUrl', function(event) {        //videoBrowse.php - Video browse thumbnail displays the video. Video-related event handlers are then attached.
    var vidFileName = $(this).attr("href");                                         //Fetching the video name from the href attribute
    isSubAvailable(vidFileName);                                                    //Function sets the user name space flag
    isChapAvailable(vidFileName);                                                   //Function sets the user name space flag
   
   $.get('serverlogic/serverLogic.php?currentVid=' + vidFileName, function(data) {  //Loading in the video player as well as the video into its respective <div>
      $("#videoDisplay").html(data);                                                
      
      if(user.isSubAvailable == "true") {
        $("#trackSubs")[0].onload = function() {                                    //On subtitle tracks load...
          displayExternalVideoSubtitles(this);                                      //Attaches tracks subtitles to the video element
        }                                                                           
      }
      
      if(user.isChapAvailable == "true") {
        $("#videoCurrent")[0].oncanplay = function() {                              //On video canplay.       
          displayVideoChapters(this);                                               //Attaches tracks chapters to the video element 
          
        }     
      }
      
      if(user.accessLevel == "Student") {                                           //If student: Video watch time will be monitored
        $("#videoCurrent")[0].onplay = function() {
          videoViewTimer(this, vidFileName, "onplay")                               //Keeps track of how many seconds of the video the user sees.
        }                                                                           
                                                                                    
        $("#videoCurrent")[0].onpause = function() {                                
          videoViewTimer(this, vidFileName, "onpause")                              //Keeps track of how many seconds of the video the user sees.
        }
      }
      
      if(user.accessLevel == "Teacher") {                                           //If teacher: Subtitle editing available.
       
        $('#displayVideoSubsBottom').mouseenter(function() {                        //Mouse over will highlight the edit area
           if(user.isSubsEditBoxOpen === false) { 
             $("#displayVideoSubsBottom").addClass('mouseenterVideoSubsBottom');
           }
         })
               
        $('#displayVideoSubsBottom').mouseleave(function() {                        //Mouse leave will remove the highlighted area.
          if(user.isSubsEditBoxOpen === false) {
            $("#displayVideoSubsBottom").removeClass('mouseenterVideoSubsBottom');
          }
        })
            
        $('#displayVideoSubsBottom').click(function() {                             //Clicking the video subtitles will open the editing area.
          if(user.isSubsEditBoxOpen === false) {
            $("#displayVideoSubsBottom").removeClass('mouseenterVideoSubsBottom');  //Remove smaller blue dashed square.
            $("#displayVideoSubsBottom").addClass('onclickVideoSubsBottom');        //Add big blue dashed square.
            $("#bottomSubsView").addClass('hideMe');                                //Hide the display-div.
            $("#bottomSubsEdit").removeClass('hideMe');                             //Show the edit-div.
            
            user.isChapsEditBoxOpen = true;                                         //Not allowing both edit boxes to be open at the same time.           
            user.isSubsEditBoxOpen = true;                                          //Tracks edit dashed square is now open.
            var editSubArea = $("#subsTextArea");
            var tracksFilePath = $("#trackSubs").attr("src");
            
            getLiveTrackContents(editSubArea, tracksFilePath, "subs");              
            $("#subsTextArea").addClass('liveSubtitles');                           //Make the textArea element fit the div after its magnified.
        
            $('body').click(function(event) {                                       //Event listener is added upon clicking in the subtitle area and can only trigger once.$('#displayVideoSubsBottom').click(false);                             //Event listener will not trigger if clicking inside the subtitle area or clicking on any of the subtitle area contents.
              var container = $("#displayVideoSubsBottom");
              if(!container.is(event.target) &&                                     //If the click target is not this div
                  container.has(event.target).length === 0 &&                       //&& the click target is not children of this div
                  event.target.id != "videoCurrent") {                              //&& the click target is not the video player
                                                                                    
                 $("#bottomSubsView").removeClass('hideMe');                        //Show the display-span.
                 $("#bottomSubsEdit").addClass('hideMe');                           //Hide the edit-span.
                 $("#displayVideoSubsBottom").removeClass('onclickVideoSubsBottom');//Remove blue dashed square.
                 
                 $(this).off('click');                                              //Remove event listener after first click.
                 user.isSubsEditBoxOpen = false;                                    //Tracks edit dashed square is now closed.
                 user.isChapsEditBoxOpen = false;
                 saveLiveTracks("subs", tracksFilePath);                            //Saves the contents in <textarea> to its respective file.
                 //re-load subtitles to player to see updates!
               }           
            });
          
          }
        });
       
       
       
       $('#displayVideoSubsRight').mouseenter(function() {                          //Mouse over chapter list will highlight the edit area
          if(user.isChapsEditBoxOpen === false) { 
            $("#rightChapsEdit").addClass('mouseenterVideoSubsRight');
          }
        })
        
       $('#displayVideoSubsRight').mouseleave(function() {                          //Mouse leave chapter list will remove the highlighted area.
         if(user.isChapsEditBoxOpen === false) {
           $("#rightChapsEdit").removeClass('mouseenterVideoSubsRight');
         }
       })
        
       $('#rightChapsEdit').mouseenter(function() {                                 //Mouse over chapter edit area will highlight the edit area
         if(user.isChapsEditBoxOpen === false) {                                    
           $("#rightChapsEdit").addClass('mouseenterVideoSubsRight');               
         }                                                                          
       })                                                                           
                                                                                    
       $('#rightChapsEdit').mouseleave(function() {                                 //Mouse leave chapter edit area will remove the highlighted area.
         if(user.isChapsEditBoxOpen === false) { 
           $("#rightChapsEdit").removeClass('mouseenterVideoSubsRight');
         }
       })

       
       $('#rightChapsEdit').click(function() {
         if(user.isChapsEditBoxOpen === false) {
           
           $("#chapsTextArea").removeClass('hideMe');                               //Show the edit-div.
           user.isChapsEditBoxOpen = true;
           user.isSubsEditBoxOpen = true;                                           //Not allowing both boxes to open at the same time.
           var editChapArea = $("#chapsTextArea")
           var tracksFilePath = $("#trackChaps").attr("src");
           
           getLiveTrackContents(editChapArea, tracksFilePath, "chaps");
           $("#chapsTextArea").addClass('liveChapters');                            //Make the textArea element fit the div after its magnified.

           $('body').click(function(event) {                                        //Event listener is added upon clicking in the subtitle area and can only trigger once.
             var container = $("#rightChapsEdit");
             if(!container.is(event.target) &&                                      //If the click target is not this div
                 container.has(event.target).length === 0 &&                        //&& the click target is not children of this div
                 event.target.id != "videoCurrent") {                               //&& the click target is not the video player
                 
                 $("#chapsTextArea").addClass('hideMe');                            //Hide the edit-div.
                 $("#rightChapsEdit").removeClass('mouseenterVideoSubsRight');      //Remove blue dashed square.
                 $(this).off('click');                                              //Remove event listener after first click.
                 user.isChapsEditBoxOpen = false;                                   //Tracks edit dashed square is now closed.
                 user.isSubsEditBoxOpen = false;
                 saveLiveTracks("chaps", tracksFilePath);                           //Saves the contents in <textarea> to its respective file. 
                //re-load chapters to player to see updates!
             }           
           });
         
         
         }         
       }); 
      
      }
    });
    event.preventDefault();                                                         //Prevents default href="" destination from thumbnail links.
  });
  
  $("#formTracksUpload").submit(function(event) {                                   //videoTracksUpload.php - "Add subtitles & chapters" upload button uploads tracks-file and attaches it to the selected video.
    var formData = new FormData($(this)[0]);                                        //Using FormData will include both data inputs files within the selected form
    $.ajax({                                                                        
      url: "serverlogic/serverLogic.php",                                               
      type: "POST",                                                                 
      data: formData,                                                               //$_post['tracksFile'] && $_post['tracksVidid']
      processData: false,                                                           //jQuery will not process the data, converting the values into strings
      contentType: false,                                                           //jQuery will not set contentType
      error: function(xhr, status, error) {
        alert(error);
      },
      success: function(result) {                                                   
        $("#formTracksUpload")[0].reset();                                           //Resets the radio list
        $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePersonal=true"); //Loads updated video browse section.
      }
    });
    event.preventDefault();                                                         //Prevent the submit from page refresh.
  });

  $("#formVideoUpload").submit(function(event) {                                    //videoUpload.php - Submit will trigger even if button is activated by ENTER key and not only on CLICK
    var formData = new FormData($(this)[0]);                                        //Using FormData will include both data inputs files within the selected form
    $.ajax({                                                                        
      url: "serverlogic/serverLogic.php",                                               
      type: "POST",                                                                 
      data: formData,							                                                  
      processData: false,                                                           //jQuery will not process the data, converting the values into strings
      contentType: false,                                                           //jQuery will not set contentType, 
      error: function(xhr, status, error) {
        alert(error)
      },
      success: function(result) {                                                            
        $("#formVideoUpload")[0].reset(); 																	              //Empty the input fields after submit.
        $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePersonal=true");      //Updates video browse section.
        $("#tracksRadioList").load("serverlogic/serverLogic.php?browseTracksRadioList=true"); //Updates tracks radio list.
        $("#watchRadioList").load("serverlogic/serverLogic.php?watchlistContents=true");      //Updates watch radio list.
      }
    });
  event.preventDefault();                                                           //Prevents submitting the normal way with page refresh
  });
      
  $("#videoFile").change(function() {                                               //videoUpload.php - Screenshots each uploaded video as a thumbnail
    var img  = document.getElementById("videoThumbnail");
    var file = this.files[0];                                                       //All chosen files to be uploaded exists in the files array
    video = document.createElement('video');                                        //Creates a <video> element which is to play the selected video
    video.src = URL.createObjectURL(file);                                          //<video> element receives a source to the chosen file
    video.addEventListener('canplay', captureImage);                                //When the video has been loaded enough to start play (canplay), captureImage() is called
     
    function captureImage() {
      var canvas = document.createElement('canvas');                                //Creates a <canvas> element 
      var ctx = canvas.getContext('2d');                                            //getContext('2d') methods allows drawing inside the <canvas> element
      ctx.drawImage(video, 0, 0, canvas.width, canvas.height);                      //The <video> element is drawn into the <canvas> element.
      img.value = canvas.toDataURL('image/png');                                    //The drawn picture's base64 code is set into the hidden element's value
      video.removeEventListener('canplay', captureImage);                        
    }
  });
  
  $("#formPlaylistEdit").submit(function(event) {                                   //playlistManage.php - "save changes" button saves NEW playlist OR (not both) updates OLD playlist
    var newList = $("[name='newUserPlaylist']")[0].value;                           //Saving the input element with name="newUserPlaylist"'s value
    var vidList = $("[name='videoList[]']");                                        
    event.preventDefault();        																					        //Prevents submit button to page refresh.
                                                                                    
   if(newList.length > 0) {                                                         //If something has been typed into "new playlist", only this will be saved
     $.ajax({
       url: "serverlogic/serverLogic.php",
       type: "POST",
       data: { newUserPlaylist : newList },
       success: function() {
         $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");    //Updates the playlist display with the new playlist..
         $("#formPlaylistEdit")[0].reset();     
         $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePublic=true"); //Public video browse re-loads automatically.
       }
     });
       
    } else if(newList.length == 0) {                                                //Else save possible playlist order changes
     var changeList = [];
     for(var ii = 0, len = vidList.length; ii < len; ++ii) {
       changeList[ii] = vidList[ii].value;                                      
     }
     $.ajax({
       url: "serverlogic/serverLogic.php",
       type: "POST",
       data: { newList : changeList },                                           
       success: function(result) {
         $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");    //Updates the playlist display with the new playlist..
         $("#formPlaylistEdit")[0].reset(); 
       }
     });
    }
  });
  
  $("ul#videoPlaylist").on('click', 'button.moveDown', function() {                 //playlistManage.php - Arrow down buttons, using .on instead of .click due to dynamically generated HTML
    numPlaylists = document.getElementsByClassName('playlistTitle').length;         //Counts number of playlists
    videoList = [];                                                                 //Array will contain node list objects for each hidden input element
    for(var ii = 1; ii <= numPlaylists; ii++) {                                     //Fetches all hidden elements. Separates element classes into array indexes.
      videoList[ii] = document.querySelectorAll(".playlistNum" + ii);               //Creates a NodeList object for each playlist, containing all the videos
    }                                                                               
    /*                                                                              
       numPlaylists and videoList would preferably get their data after             
       the playlist has been loaded instead of when being clicked.                  
       This way the two buttons would work with same variables.                     
       Couldn't find a solution to this.                                            
    */                                                                        
    var btnId = (this.id);                                                          //Arrow down button IDs "moveDown$ii-$jj" where $ii is playlist 1-N & $jj is video 0-M per playlist
    btnId = btnId.slice(8);                                                         //Removing string "moveDown" from ID.
    var vidId = btnId.split("-");                                                   //Extracting $ii and $jj from the ID into two indexes. The separator will ensure correct id no matter how many digits.
    var ii = vidId[0];                                                              //Assigning the index values to variables ii and jj.
    var jj = (vidId[1] - 1);                                                        //Since the node list objects starts at index 0, and we 1, we reduce by 1
                                                                                    //The if-statement below increments <hidden input> values according to their position in the play list
    if(videoList[ii][jj].value < videoList[ii].length) {                            //index [length] means last video in the play list, you cannot move a video further down while being at [length]
      videoList[ii][jj].value++;                                                    //If not at [length], increment value (which means move down)
      var vidBelow = videoList[ii][jj].value;                                       //The video's new incremented value will now equal some other video's current value..
      for(var kk = 0; kk < videoList[ii].length; kk++) {                            //..find that video and decrement its value (move it up).
        if(videoList[ii][kk].value == vidBelow && videoList[ii][kk] != videoList[ii][jj]) {
          videoList[ii][kk].value--;
        }     
      }
    }
   moveDown($('#playlistVideoNum' + btnId));                                   //And then call the function moving the visible list item on your screen.
  });
  
  $("ul#videoPlaylist").on('click', 'button.moveUp', function() {                   //playlistManage.php - Arrow up buttons, using .on instead of .click due to dynamically generated HTML
    /* See comments for "arrow down buttons"; this one is reversed */    
    numPlaylists = document.getElementsByClassName('playlistTitle').length;    
    videoList = [];                                                            
    for(var ii = 1; ii <= numPlaylists; ii++) {                                
      videoList[ii] = document.querySelectorAll(".playlistNum" + ii);          
    }
    
    var btnId = (this.id);                                                      
    btnId = btnId.slice(6);                                                      
    var vidId = btnId.split("-");                                                
    var ii = vidId[0];                                                           
    var jj = (vidId[1] - 1);
    
    if(videoList[ii][jj].value > 1) {                                             
      videoList[ii][jj].value--;                                                        
      var vidAbove = videoList[ii][jj].value;
      for(var kk = 0; kk < videoList[ii].length; kk++) {
        if(videoList[ii][kk].value == vidAbove && videoList[ii][kk] != videoList[ii][jj]) {
          videoList[ii][kk].value++;
        }     
      }
    }
    moveUp($('#playlistVideoNum' + btnId));
  });
  
  $("ul#videoPlaylist").on('click', 'button.playlistDeleteBtn', function() {        //playlistManage.php - Delete playlist button, using .on instead of .click due to dynamically generated HTML
    if(confirmDelete()) {                                                           //If alert box is accepted
      var plidId = $(this).attr("data-playlistId");                                 //Get playlistvideo ID from data-attribute
      $.ajax({
        url: "serverlogic/serverLogic.php",
        type: "POST",
        data: { deletePlaylist : plidId },                                          //Send playlistvideo ID to PHP function
        error: function(xhr, status, error) {
          alert(error);
        }, 
        success: function(result) {
          $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");   //Load updated playlists on complete
          $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePublic=true");//Public video browse re-loads automatically
        }
      });
    }
  });
  
  $("ul#videoPlaylist").on('click', 'button.playlistvideoDeleteBtn', function() {   //playlistManage.php - Delete playlistvideo button
    if(confirmDelete()) {                                                           //If alert box is accepted
      var plidVidId = $(this).attr("data-plidVidId");                               //Get playlistvideo ID from data-attribute
      $.ajax({
        url: "serverlogic/serverLogic.php",
        type: "POST",
        data: { deletePlaylistVideo : plidVidId },                                  //Send playlistvideo ID to PHP function
        error: function(xhr, status, error) {
          alert(error);
        }, 
        success: function(result) {
          $("#videoPlaylist").load("serverlogic/serverLogic.php?videoPlaylist=true");   //Load updated playlists on complete
        }
      });
    }
  });
  
  $("#watchRadioList").on('change', 'input[name=watchlistVidId]:checked', function() { //videoWatchlist.php - When selecting a radio button, appropriate watchfile is output
    var vidId = $(this).val();
    $("#watchlistContents").load("serverlogic/serverLogic.php?watchlistVidId=" + vidId);
  });

  $("#videoWatchlistSidepanel").click(function() {                                  //videoWatchlist.php - When opening from sidepanel, personal videos are display as well
    $("#globalVideoBrowse").load("serverlogic/serverLogic.php?browsePersonal=true");
  });
  
  $("#resetWatchfileBtn").click(function() {                                        //videoWatchlist.php - Resets a watchfile on button click.
    if(confirmDelete()) {
      var videoId = $("#watchRadioList input[name=watchlistVidId]:checked")[0].value; //Extracting 
      $.ajax({
        url: "serverlogic/serverLogic.php",
        type: "POST",
        data: { resetWatchfile : videoId },
        error: function(xhr, status, error) {
          alert(error);
        },
        success: function(result) {
          $("#watchlistContents").load("serverlogic/serverLogic.php?watchlistVidId=" + videoId);
        }
      }); 
    }
  });
  
  $("a#userSignOut").click(function(event) {                                        //Signs the user out.
    $.get("serverlogic/serverLogic.php?signout=true", function() {
      location.reload();
    });
    event.preventDefault();
  });
  
  
});
