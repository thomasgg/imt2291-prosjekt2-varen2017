function getUserAccessLevel() {                                                //Sets the user access level for this script file.
  $.get("serverlogic/serverLogic.php?userAccessLevel=true", function(result) {
    user.accessLevel = result; 																				         //Getting user access status as string
  });
}

function isSubAvailable(vidFileName) {                                         //Returns true / false for subtitles existence
  $.get("serverlogic/serverLogic.php?isSubAvailable=" + vidFileName, function(result) {
    user.isSubAvailable = result;
  });
}

function isChapAvailable(vidFileName) {                                        //Returns true / false for chapters existence
  $.get("serverlogic/serverLogic.php?isChapAvailable=" + vidFileName, function(result) {
    user.isChapAvailable = result;
  });  
}

function getLiveTrackContents(txtAreaId, tracksFilePath, tracksType) {         //Gets tracks contents from file and puts it in an enditable textarea.
  if(tracksType == "subs") {
    $.get("serverlogic/serverLogic.php?trackSubContents=" + tracksFilePath, function(result) {
        $(txtAreaId).val(result); 	
    });
  } else if (tracksType == "chaps") {
    $.get("serverlogic/serverLogic.php?trackChapContents=" + tracksFilePath, function(result) {
      $(txtAreaId).val(result); 	
    });
 }
}

function saveLiveTracks(tracksType, tracksFilePath) {                          //Saves tracks contents in editable textarea.
  if(tracksType == "subs") {
    var data = $("#subsTextArea").val();
    $.post("serverlogic/serverLogic.php", 
      { 
      tracksContent : data,
      tracksFilePath : tracksFilePath
      });
    
  } else if(tracksType == "chaps") {
    var data = $("#chapsTextArea").val();
    $.post("serverlogic/serverLogic.php", 
      { 
      tracksContent : data,
      tracksFilePath : tracksFilePath
      });
  }
}

function confirmDelete() {                                                     //Returns true & performs deletion or false & cancels deletion.
  var check = confirm("Are you sure you wish to perform this action?");
  return check === true;                                                       
}

function displayExternalVideoSubtitles(trackEl) {                              //videoDisplay.php Displays subtitles outside the video element
  var track     = trackEl;                                                    
  var textArea  = document.getElementById("bottomSubsView");                  //The area to output the tracks
 
  trackEl.addEventListener("cuechange", function() {                           //Function kicks in for each event: cuechange
    var currTrack = this.track;                                                 
    currTrack.mode = "hidden";                                                 //Disables in-screen tracks
    var currCues = currTrack.activeCues;                                                                                       
    if(currCues.length > 0) {               
      textArea.replaceChild(currCues[0].getCueAsHTML(), textArea.firstChild);  //Replaces last text with new. At first execution, the '&nbsp' is replaced.
  //  textArea.appendChild(currCues[0].getCueAsHTML());                        //Appends next cue text
  //  textArea.appendChild(document.createElement("br"));                      //Appends new line
    }
  });
  /* Note: Subtitles are not removed when the cue time has run out. 
     Fix this with "video.textTrack[1].cues[ii].endTime" */
}

function displayVideoChapters(video) {                                         //videoDisplay.php Displays clickable chapter title links outside the video element
  if(video.textTracks.length > 0) {                                            //If text tracks exists
    var textTrack    = "";
    var errFlag      = true;
    var chapterList  = document.getElementById("rightChapsView");              //The list in the area to output the tracks 
     
    for(var ii = 0; ii < video.textTracks.length; ii++) {                      //Iterates the text tracks attched to the element and finds the chapter file
      if (video.textTracks[ii].kind === "chapters") {                          //If the track is a chapter file..
        textTrack = video.textTracks[ii];                                      //..store the file..
        errFlag = false;                                                       //..and allow further processing
      }
    }
    
    if(chapterList.firstChild) {                                               //If chapter list already exists in the DOM; set error
      errFlag = true;   
    /* 
       Function re-appends all
       cue chapter titles for each
       eventListener click. This is
       a workaround. Would remove 
       event listener instead time
       is too limited atm.
    */       
    }
    
    if(errFlag === false) {                                                    
      var cue = textTrack.cues;     
      for(var ii = 0; ii < textTrack.cues.length; ii++) {                      //For all the file's cues.
        var span       = document.createElement("span");                       //Create a span element to hold output
        span.innerHTML = "<li>(" + cue[ii].startTime + "s) - " + cue[ii].text + "</li>";   //Output "(mm.ss) - ChapterTitle ", formating can be improved
        span.setAttribute("data-startTime", cue[ii].startTime)                 //Save cue time to span element
        span.addEventListener("click", seekVideoTime);                         //Add "click" event listener to span element
        chapterList.appendChild(span);                                         //Append span element to unordered list in chapterList
      }
    }
    
    function seekVideoTime() {
      video.currentTime = this.getAttribute("data-startTime");                 //videoDisplay.php Set current video timeline to this span's attribute
      if(video.paused) {                                                       //If the video is paused, start playing at new time.
        video.play();
      }
    }
  }
}

function deletePersonalVideo(element) {                                        //playlistManage.php - for each video delete button.
  if(confirmDelete()) {                                                        //If alert box is accepted  
    var videoId = element.getAttribute("data-videoId");                        //Get the video Id 
    document.getElementById("deleteVideoId").value = videoId;                  //Send the video Id to hidden form
    document.getElementById("formDeleteVideoId").submit();                     //And submit form
  }                                                                            
}                                                                              
                                                                               
function deletePersonalPlaylist(element) {                                     //playlistManage.php Deletes playlist
  if(confirmDelete()) {                                                        
    var playlistId = element.getAttribute("data-playlistId");                  
    document.getElementById("deletePlaylist").value = playlistId;              
    document.getElementById("formPlaylistDelete").submit();                    
  }                                                                            
}                                                                              
                                                                               
function moveUp(item) {                                                        //playlistManage.php up arrow
  var before = item.prev();                                                      
  item.insertBefore(before);                                                 
}                                                                              
                                                                               
function moveDown(item) {                                                      //playlistManage.php down arrow
  var after = item.next();
  item.insertAfter(after);
}
     
function videoViewTimer(vidEl, vidId, status) {                                //videoDisplay.php - Tracks amount of time watched a video
  var startTime = [];
  var endTime   = [];
  var numRanges = vidEl.played.length;                                         //number of different time ranges
  
  if(status == "onplay" || status == "onpause") {                              //Onplay & onpause triggers on pause/start/skip/end                   
    for(var ii = 0; ii < numRanges; ++ii) {
      startTime[ii] = vidEl.played.start(ii);                                  //Fetching the video seen start marks
      endTime[ii]   = vidEl.played.end(ii);                                    //Fetching the video seen end marks
    }
                             
    $.post("serverlogic/serverLogic.php",                                      //Sending the video seen time intervals to the student user object
      { 
      watchStartTime : startTime,
      watchEndTime : endTime,
      watchVidId : vidId
      });
  } 
}
