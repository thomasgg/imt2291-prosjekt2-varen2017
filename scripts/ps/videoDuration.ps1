param([string]$folderPath, [string]$fileName)
cd $folderPath                                              #folderPath uses a relative path
$thisPath = (Get-item -Path ".\" -Verbose).fullName         #Transforming to absolute path
$LengthColumn = 27                                          #27 specifies the video length
$objShell = New-Object -ComObject Shell.Application
$objFolder = $objShell.Namespace($thisPath)
$objFile = $objFolder.ParseName($fileName)
$Length = $objFolder.GetDetailsOf($objFile, $LengthColumn)
return $Length                                              #Makes output available for PHP shell_exec()