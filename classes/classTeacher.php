<?php 
class Teacher extends User {
  private $file;                                                               //Class objects

  public function __construct($db) {
    User::__construct($db);
  }
  
  protected function getWatchfileName($videoId) {                              //Returns the DB filename if given the DB video ID.
    $sql = "SELECT vidFileName FROM video WHERE uploaderId = :user
            AND videoId = :vidId";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array('user' => $this->userId, 'vidId' => $videoId));
    $res = $stmt->fetch(PDO::FETCH_ASSOC);
    $fileName = $res['vidFileName'];
    $watchfileName = "../uploads/text/" . substr($fileName, 0, -4) . "_watch.txt";   //WARNING: Filepath must be changed dependant on where its called from
    return $watchfileName; 
  } 
  
  protected function deleteWatchfile($filename) {                              //Deletes a video's watchfile upon video deletion.
    $filePath = "../uploads/text/" . $filename . "_watch.txt";                 //WARNING: Filepath must be changed dependant on where its called from
    if(file_exists($filePath)) {
      unlink($filePath);
    }
  }
  
  public function getWatchfileContents($vidId) {                               //Reads watchfile contents and outputs them
    if($this->isLoggedIn()) {                
    $watchfileName = $this->getWatchfileName($vidId);                          //gets filename from ID
      if($this->isFileAvailable($watchfileName)) {                    	       //If file exists, (empty allowed)
        $watchfile = fopen($watchfileName, 'r');
      
        if(filesize($watchfileName) > 0) {
          $contents = fread($watchfile, filesize($watchfileName));
          fclose($watchfile);
          
          $tempEntries = array();
          $tempEntries = explode("-", $contents);                              //Each student is separated by - into his/her own index
          $studentEntries = array();
          $length = count($tempEntries) - 1;                                   //Explode will treat the last "-" as a new empty index, we'll subtract this.
          
          for($ii = 0; $ii < $length; ++$ii) {
            $studentEntries[$ii] = explode("_", $tempEntries[$ii]);            //Each student entry contains "email_seconds". Separating these.
            echo $studentEntries[$ii][0] . " - " . $studentEntries[$ii][1] . "s<br>";//Printing out the values.
          }
        } else {
          echo "Watchlist empty.";
        }
      } else {
        echo "No watch list found for selected video";
      }
    }
  } 

  public function resetWatchfileContents($vidId) {                             //Erases all contents from a specific watchfile.
    if($this->isLoggedIn()) {
      $watchfileName = $this->getWatchfileName($vidId);
      file_put_contents($watchfileName, "");
    }
  }
  
  public function uploadUserFile($file = array()) {                            //Saves uploaded file to file server. Can also take a thumbnail and tracks files.
    if($this->isLoggedIn()) {
      if(!empty($_FILES)) {                                           
        $this->file = new Fileupload($this->db, $file[0], $this->userId);      //Preparing an object for upload to database
        if($this->file->isFileOk()) { 
          if($this->file->getFlagType() !== "subtitles" && 
             $this->file->getFlagType() !== "chapters"  &&
             $this->file->getFlagType() !== "thumbnail") {
               $this->file->saveFileToDisk();                                  //If object had no errors and is not a tracks or thumbnail file, save file to disk
          }          
          if(isset($file[1])) {                                                //If index 1 contains data, then an automatic thumbnail has been generated.
            $this->file->saveThumbnailToDisk($file[1]);                        //Save thumbnail to disk
          }
          if($this->file->getFlagType() === "video") {                         //If video file, then also create a watch file to track who has seen which videos.
            $this->file->saveWatchlistToDisk();
          }
        } else { echo "<br />This type of file cannot be uploaded."; }
      } else { echo "<br />No file has been selected."; }
    } else { echo "<br />You must be logged in to upload files."; }
  }
  
  public function uploadUserFileData($fileParams = array()) {                  //Array parameter accepts a variable amount of arguments
     if($this->file->isFileOk()) {
       if($this->file->getFlagType() == "video") {                             //Video must contain 5 arguments
         $this->file->getFileInfo(array('title' => $fileParams['title'],
                                          'category' => $fileParams['category'],
                                          'description' => $fileParams['description'],
                                          'tags' => $fileParams['tags']));
    } else if($this->file->getFlagType() == "image") {                         //Image must contain x arguments
       //then create an image information array
    } else if($this->file->getFlagType() == "text") {                          //Text must contain y arguments
       //then create a text information array
    }
      $this->file->saveFileInfoToDb();
    }
  }
  
  public function deleteUserVideo($vidId) {                                    //Deletes a video belonging to the user specified by title
    if($this->isLoggedIn()) {   
      $sql = "SELECT vidFileName FROM video WHERE uploaderId = :user
              AND videoId = :vidId";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array('user' => $this->userId, 'vidId' => $vidId));      
      if($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $fileName = substr($res['vidFileName'], 0, -4);                        //Removes ".mp4"
        $filePath = "../uploads/videos/" . $fileName . ".mp4";                 //WARNING: Filepath must be changed dependant of from where it is called!
        unlink($filePath);
        $this->deleteUserImage($fileName, true); //Temporary until I get proper tables for texts & images
        $this->deleteUserText($fileName, true);  //Temporary until I get proper tables for texts & images
        $this->deleteWatchfile($fileName);
      }
      
      $sql = "DELETE FROM video WHERE uploaderId = :user 
              AND videoId = :vidId";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array('user' => $this->userId, 'vidId' => $vidId));  
    }
  }
  
  public function deleteUserImage($imgId, $isThumbnail) {                      //Deletes an image belonging to user. Takes additional bool parameter if image is a thumbnail.
    if($this->isLoggedIn()) {
      if($isThumbnail) {                                                       //If thumbnail, file name equals video file name
        $filePath = "../uploads/images/" . $imgId . ".png";                    //WARNING: Filepath must be changed dependant of from where it is called!
        unlink($filePath);                                                     
      } else if (!($isThumbnail)) {
        /* Not implemented */
        $sql = "";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array('user' => $this->userId, 'imgId' => $imgId)); 
      }
    }
  }
  
  public function deleteUserText($textId, $isVideoTrack) {                     //Deletes a text file belonging to user. Takes additional bool parameter if text is a track file.
    if($this->isLoggedIn()) {
      if($isVideoTrack) {
        $filePath = "../uploads/transcripts/" . $textId . "_subs_en.vtt";      //WARNING: Filepath must be changed dependant on where its called from
        if(file_exists($filePath)) {
          unlink($filePath);
        } 
        $filePath = "../uploads/transcripts/" . $textId . "_chaps_en.vtt";     //WARNING: Filepath must be changed dependant on where its called from 
        if(file_exists($filePath)) {
          unlink($filePath);
        }

      } else if (!($isVideoTrack)) {
        /* Not implemented */
        $sql = "";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array('user' => $this->userId, 'textId' => $textId)); 
      }
    }
  }
 
  public function addTracksToVideo($vidId) {                                   //Connects a tracks .vtt file to a specified video.
    if($this->isLoggedIn()) {
      $sql = "SELECT vidFileName FROM video 
              WHERE uploaderId = :userId
              AND   videoId    = :vidId";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array('userId' => $this->userId, 'vidId' => $vidId));
      if($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $this->file->addTracksToVideo($res['vidFileName']);
        $this->file->saveFileToDisk();
      } else {
        echo "DEBUG: Video not found";
      }
    }
  }
  
  public function updateLiveTracksFile($contents, $filePath) {                 //Writes contents and saves tracks file from live-editing input.
    if($this->isLoggedin()) {
      $tracksfile = fopen($filePath, 'w');                                     //Opens the file in write-only mode.
      fwrite($tracksfile, $contents);
      fclose($tracksfile);
    }
  }
  
 /* public function addThumbnailToVideo($vidId) {                                //Not yet implemented, manual thumbnail upload
    //do as with tracks in function above. Already implemented..
    //..file->getFlagType() 
    //..user->uploadUserFile() parts to get started
    //Save object with same video file name, replace mp4 THEN call saveFileToDisk()
  } */
 
  
}
?>