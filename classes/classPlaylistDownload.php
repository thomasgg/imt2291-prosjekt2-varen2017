<?php
class PlaylistDownload {
  private $db;
  private $vidId = array(), $vidPos = array(), $vidTitle = array(),
           $vidSubj = array(), $vidThumbnail = array();
  private $vidTags = array(), $vidDescr = array(), $vidDur = array(), 
           $vidFileName = array(), $transFileName = array();
  private $plistVidId = array();
  private $pListName, $pListId, $lastVideo;
  
  
  public function __construct($db, $pListId, $name, $pos, $vidId, $title,      //Each constructor call creates a new playlist
                              $cat, $descr, $dur, $fName, $tFName, $tags,
                              $plistVidId) {                          
    $this->db = $db;
    $this->pListId = $pListId;
    $this->pListName                         = $name;                
    $this->lastVideo                         = 1;                              //Keeps track of last used index, starts at 1
    if($vidId > 0) {                                                           //If vidId is larger than 0, then the playlist contains videos
      $this->vidId[$this->lastVideo]         = $vidId;
      $this->vidPos[$this->lastVideo]        = $pos;
      $this->vidTitle[$this->lastVideo]      = $title;
      $this->vidSubj[$this->lastVideo]       = $cat;
      $this->vidDescr[$this->lastVideo]      = $descr;
      $this->vidDur[$this->lastVideo]        = $dur;
      $this->vidFileName[$this->lastVideo]   = $fName;
      $this->transFileName[$this->lastVideo] = $tFName;
      $this->vidTags[$this->lastVideo]       = $tags;
      $this->plistVidId[$this->lastVideo]    = $plistVidId;
    } else {                                                                   //Else its an empty playlist
      $this->lastVideo                       = 0;
    }

  }
   
  public function addVideo($id, $pos, $title, $cat, $descr, $dur,              //Each addVideo call adds a video to an existing playlist
                           $fName, $tFName, $tags, $plistVidId) {
    $this->lastVideo++;
    $this->vidId[$this->lastVideo]         = $id;
    $this->vidPos[$this->lastVideo]        = $pos;
    $this->vidTitle[$this->lastVideo]      = $title;
    $this->vidSubj[$this->lastVideo]       = $cat;
    $this->vidDescr[$this->lastVideo]      = $descr;
    $this->vidDur[$this->lastVideo]        = $dur;
    $this->vidFileName[$this->lastVideo]   = $fName;
    $this->transFileName[$this->lastVideo] = $tFName;
    $this->vidTags[$this->lastVideo]       = $tags;
    $this->plistVidId[$this->lastVideo]    = $plistVidId;
  }
  
  public function sortPlistByPosition() {                                      //Sorts all the arrays ascending using the videoPos array
      $temp1 = $temp2 = $temp3 = $temp4 = $temp5 =              
           $temp6 = $temp7 = $temp8 = $temp9 = 
           $temp10 = array(); 

   for($ii = 1; $ii <= $this->lastVideo; $ii++) {                              //This loop sorts all arrays..
      $temp1[$this->vidPos[$ii]]  = $this->vidId[$ii];                          //..using distribution counting..
      $temp2[$this->vidPos[$ii]]  = $this->vidTitle[$ii];                       //..where the video position data..
      $temp3[$this->vidPos[$ii]]  = $this->vidSubj[$ii];                        //..are being used as the index numbers..
      $temp4[$this->vidPos[$ii]]  = $this->vidDescr[$ii];                       //..in the temporary arrays
      $temp5[$this->vidPos[$ii]]  = $this->vidDur[$ii];
      $temp6[$this->vidPos[$ii]]  = $this->vidFileName[$ii];
      $temp7[$this->vidPos[$ii]]  = $this->transFileName[$ii];
      $temp8[$this->vidPos[$ii]]  = $this->vidTags[$ii];
      $temp9[$this->vidPos[$ii]]  = $this->vidPos[$ii];
      $temp10[$this->vidPos[$ii]] = $this->plistVidId[$ii];
    }
   
   for($ii = 1; $ii <= $this->lastVideo; $ii++) {                             //Moving values in the temporary arrays..    
      $this->vidId[$ii]         = $temp1[$ii];                                     //..back into the private arrays
      $this->vidTitle[$ii]      = $temp2[$ii];
      $this->vidSubj[$ii]       = $temp3[$ii];
      $this->vidDescr[$ii]      = $temp4[$ii];
      $this->vidDur[$ii]        = $temp5[$ii];
      $this->vidFileName[$ii]   = $temp6[$ii];
      $this->transFileName[$ii] = $temp7[$ii];
      $this->vidTags[$ii]       = $temp8[$ii];
      $this->vidPos[$ii]        = $temp9[$ii];   
      $this->plistVidId[$ii]    = $temp10[$ii];
    }
  }
  
  public function getPlistVideoId() {                                               
    return $this->plistVidId;
  }
  
  public function getVideoId() {                                               
    return $this->vidId;
  }
  
  public function getPlistNumVideos() {                                             //Returns number of videos in the playlist 
    return $this->lastVideo;
  }
  
  public function getPlistVideoTitle() {
    return $this->vidTitle;
  }
 
  public function getPlistVideoFileName() {
    return $this->vidFileName;
  }
  
  public function getPlistVideoTransFileName() {
    return $this->transFileName;
  }
  
  public function getPlistName() {
    return $this->pListName;
  }

  public function getPlistId() {
    return $this->pListId;
  }
  
  public function getPlistVideoCategory() {
    return $this->vidSubj;
  }  
  
  public function getPlistVideoDescription() {
    return $this->vidDescr;
  }
  
  public function getPlistVideoDuration() {
    return $this->vidDur;
  } 
  
  public function getPlistVideoTags() {
    return $this->vidTags;
  }
  
  public function getPlistVideoThumbnail() {                                   //Iterates through video file name array and replaces mp4 with png
    for($ii = 1; $ii <= $this->lastVideo; $ii++) {
      $this->vidThumbnail[$ii] = (substr($this->vidFileName[$ii], 0, -3) . "png");
    }
    return $this->vidThumbnail;                                                //Returns thumbnail name
  }
   
}

?>