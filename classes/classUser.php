<?php
class User {
  protected $email, $firstName, $lastName, $pwdHash, $tableName, $userId;      //String & integer
  protected $db;                                                               //pdo db-object is accessible by children
  private $cookieRM, $playlist;                                                //Array
  protected $isLoggedIn, $isAdmin, $isTeacher, $isStudent, $debugFlag=true;      //Bool

  public function __construct($db) {                                           //Constructor handles new users / sessions / cookies 
    if(!isset($_SESSION)) { 																									 //Session check is nescessary for child constructors
      session_start();
    }
    $this->db = $db;
    if(isset($_SESSION['cookieId'])) {                                        //If session exists == inlogged
        $this->getUserData();
    } else if(isset($_COOKIE['rememberme'])) {                                 //If cookie is set in browser
        $this->cookieRM = array();
        $this->cookieRM = explode(";", $_COOKIE['rememberme']);
        $sql = "SELECT cookieId, series, token FROM cookierm
                WHERE cookieId=? AND series=?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($this->cookieRM[0], $this->cookieRM[1]));
        if($row = $stmt->fetch()) {                                            //If cookie is also set in DB
            if($row['token'] == $this->cookieRM[2]) {                          //And cookies matches
                $_SESSION['cookieId'] = $row['cookieId'];
                $this->getUserData();
                $this->rememberMe();
                
            } else {
              echo "Unknown error, try again later.";                          //User has cookie, DB does not.
            }
        }
    } else {                                                                   //If unknown user
        $this->emptyUserData();
    }
  }

  protected function existsInTable($tableName, $colName) {                     //True if colName has value in table
    $colValue = "";
    switch($colName) {
      case "userId":     $colValue = $this->userId;    break;
      case "cookieId":   $colValue = $this->userId;    break;
      case "uploaderId": $colValue = $this->userId;    break;
      case "email":      $colvalue = $this->email;     break;
      case "firstName":  $colValue = $this->firstName; break;
      case "lastName":   $colValue = $this->lastName;  break;
      default: echo "Your call to 'existsInTable()' failed due to bad argument";
               return -1;
    }
    $sql = "SELECT " . $colName . " FROM " . $tableName .
           " WHERE " . $colName . " = :" . $colName;                           //Ex: "SELECT userId FROM user WHERE userId = :userId"
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array(':' . $colName => $colValue));                        //Ex: execute(:userId => $this->userId)
    return (($row = $stmt->fetch()) ? TRUE : FALSE);
  }

  protected function getUserData() {                                           //Get user data from user database
    $this->userId = $_SESSION['cookieId'];
    $sql          = "SELECT firstName, lastName, email,
                            isAdmin, isTeacher, isStudent
                     FROM user WHERE userId = :userId";
    $stmt         = $this->db->prepare($sql);
    $stmt->execute(array(':userId' => $this->userId));
    if($row = $stmt->fetch()) {
        $this->isLoggedIn  = TRUE;
        $this->firstName   = $row['firstName'];
        $this->lastName    = $row['lastName'];
        $this->email       = $row['email'];
        if($row['isAdmin'] == 'y') {
          $this->isAdmin = true;
        }
        if($row['isTeacher'] == 'y') {
          $this->isTeacher = true;
        }
        if($row['isStudent'] == 'y') {
          $this->isStudent = true;
        }
    }
  }

  protected function deleteRMCookie() {                                        //Deletes rememberme (RM)-cookie
    if($this->existsInTable("cookierm", "cookieId")) {
        $sql  = "DELETE FROM cookierm WHERE cookieId = :userId";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array('userId' => $this->userId));
        unset($_COOKIE['rememberme']);
        setcookie('rememberme', '', time() - 3600, '/');
    } 
  }

  protected function findUserInDb($email, $pwd) {                              //If user match; sets cookieId and returns true
    $sql  = "SELECT userId, password FROM user WHERE email = :email";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array(':email' => $email));
    if($row = $stmt->fetch()) {                                                //If email exists in db
      if(password_verify($pwd, $row['password'])) {                            //If password matches in db
        $this->userId = $row['userId'];                                        //Set cookieId and return true
        return TRUE;
        }
    } else {
        return FALSE;                                                          //else return false
    }
  }

  protected function emptyUserData() {                                         //Removes all user data
    $this->isLoggedIn = $this->isAdmin = $this->isTeacher 
                      = $this->isStudent = FALSE;
    $this->firstName  = $this->lastName = $this->email 
                      = $this->pwdHash = $this->tableName = "";
    $this->userId         = -1;
  }

  protected function testInput($input) {                                       //Checks for bad input, prevents XSS
    $input = trim($input);                                                     //Removes extra spaces
    $input = stripslashes($input);                                             //Removes slashes
    $input = htmlspecialchars($input);                                         //All characters interpreted as HTML entities
    return $input;
  }

  public function isFileAvailable($filePath) {                                 //Bool: Returns true if file exists.
    return (file_exists($filePath)) ? true : false;                            //This function is pretty useless after second thoughts...
  }
  
  public function logIn($email, $pwd) {                                        //Logs user in if pwd&email matches user db
    if(($this->isLoggedIn == FALSE) && ($this->findUserInDb($email, $pwd) == TRUE) ) {
      $_SESSION['cookieId'] = $this->userId;
      $this->getUserData();
    } else if ($this->isLoggedIn == TRUE) {
      echo "<br />You are already logged in.";
    } else {
      echo "<br />Wrong username/password.";
    }
}

  public function logOut() {                                                   //Removes sessions, cookie & user data
    if($this->isLoggedIn == TRUE) {
        session_unset();
        session_destroy();
        if($this->isStudent()) {
          unset($_SESSION['watchTime']);
        }
        $_SESSION['watchTime'] = 0;
        if(isset($_COOKIE['rememberme'])) {
          $this->deleteRMCookie();
        }
        $this->emptyUserData();
    } else if ($this->isLoggedIn == FALSE) {
        "You are not logged in.";
    }
  }

  public function rememberMe() {                                               //Call after login(); Sets up OR updates persistent login
    if($this->isLoggedIn()) {                                              
      $secPerDay = 60 * 60 * 24;
      $token = password_hash($this->userId . (time()), PASSWORD_BCRYPT);
      if($this->existsInTable("cookierm", "cookieId")) {                       //If cookie exists; update it
        $series = $this->cookieRM[1];
        setcookie("rememberme", $this->userId . ";" . $series .
                  ";" . $token, time() + $secPerDay * 30, '/');
        $sql = "UPDATE cookierm SET token=? WHERE cookieId=? AND series=?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($token, $this->userId, $series));
      } else {                                                                 //If cookie does not exist; create it
        $series = password_hash($this->firstName . (time()), PASSWORD_BCRYPT);
        setcookie("rememberme", $this->userId . ";" . $series .
                  ";" . $token, time() + $secPerDay * 30, '/');
        $sql = "INSERT INTO cookierm (cookieId, series, token)
                VALUES (?,?,?)";

        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($this->userId, $series, $token));
      }
    }
  }

  public function isLoggedIn() {                                               //Returns TRUE if logged in, false otherwise
    return $this->isLoggedIn;
  }

  public function isAdmin() {                                                  //Returns TRUE if admin flag is set, false otherwise
    return $this->isAdmin;
  }
  
  public function isTeacher() {                                                //Returns TRUE if teacher flag is set, false otherwise
    return $this->isTeacher;
  }
  
  public function isStudent() {                                                //Returns TRUE if student flag is set, false otherwise
    return $this->isStudent;
  }
  
  public function getVideoFiles(&$vidArray, $category, $searchTag="") {        //Fills array with category(personal/public/search) videos, requires Video class included
      $sql = $stmt = "";
      if($category == "personal") {                                            //If requesting personal videos
        if($this->isLoggedIn()) {                                              
          $sql = "SELECT videoId, uploaderId, title, 
                         category, description, duration, 
                         vidFileName, transFileName, tags
                  FROM video 
                  WHERE uploaderId = :uploaderId LIMIT 20";
          $stmt = $this->db->prepare($sql);
          $stmt->execute(array(':uploaderId' => $this->userId));
        }
      } else if($category == "public") {                                       //If requesting public videos..
        if($this->isLoggedIn()) {                                              //..and is logged in
          $sql = "SELECT videoId, uploaderId, title,
                        category, description, duration, 
                        vidFileName, transFileName, tags
                  FROM video
                  WHERE NOT uploaderId = :uploaderId LIMIT 20";                //Excluding videos uploaded by the user            
          $stmt = $this->db->prepare($sql);                                    
          $stmt->execute(array(':uploaderId' => $this->userId));
        } else {                                                               //If requesting public videos & not logged in
          $sql = "SELECT videoId, uploaderId, title,
                        category, description, duration, 
                        vidFileName, transFileName, tags
                  FROM video LIMIT 20";
          $stmt = $this->db->prepare($sql);                                        
          $stmt->execute();
        }
      } else if($category == "search") {
          $sql = "SELECT videoId, uploaderId, title,
                        category, description, duration, 
                        vidFileName, transFileName, tags
                  FROM video
                  WHERE tags LIKE :searchTag
                  OR title LIKE :searchTag
                  OR description LIKE :searchTag";
          $stmt = $this->db->prepare($sql);                                        
          $stmt->execute(array(':searchTag' => '%'.$searchTag.'%'));
      }
      $ii = 1;                                                                 //Starting array at index 1   
      if($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
        foreach($row as $val) {
          $vidArray[$ii++] = new VideoDownload($val['vidFileName'],
              $val['uploaderId'], $this->db, $val['title'], $val['category'],
              $val['description'], $val['duration'],
              $val['transFileName'], $val['tags'], $val['videoId']);
        }
      }  
    } 
  
  public function getVideoPlaylists(&$pListArr) {                              //Fills array with playlist objects containing playlist videos. Requires PlaylistDownload class.
    if($this->isLoggedIn()) {
      if($this->existsInTable("playlist", "uploaderId")) {                     //If user's userId is found as playlist's uploaderId
        $sql = "SELECT playlist.name            AS pListName,
                  playlist.playlistId           AS pListId,
                  playlistvideo.playlistVideoId AS pListVidId,
                  playlistvideo.videoId         AS vidId,
                  playlistvideo.videoPos        AS vidPos,
                  video.title                   AS vidTitle,
                  video.category                AS vidCat,
                  video.tags                    AS vidTags,
                  video.description             AS vidDescr,
                  video.duration                AS vidDur,
                  video.vidFileName             AS vidFName,
                  video.transFileName           AS transFName
                FROM playlist
                LEFT JOIN playlistvideo ON playlist.playlistId = playlistvideo.playlistId
                LEFT JOIN video   ON playlistvideo.videoId = video.videoId
                WHERE playlist.uploaderId = :uploaderId
                ORDER BY pListId ASC";   
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(':uploaderId' => $this->userId));
        if($row = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
          $ii = 0;                                                             //ii handles the playlist creation
          $jj = 0;                                                             //jj defines the global array indexes
          foreach($row as $val) {          
            if($val['pListId'] == $ii) {                                        //if playlistId matches ii
             $pListArr[$jj]->addVideo($val['vidId'], $val['vidPos'],            //add current video to current object
                                      $val['vidTitle'], $val['vidCat'],
                                      $val['vidDescr'], $val['vidDur'],
                                      $val['vidFName'], $val['transFName'],
                                      $val['vidTags'],  $val['pListVidId']);
            } else if($val['pListId'] > $ii) {                                 //if playlistId is larger than ii
              $ii = $val['pListId'];                                           //Set ii = playlist ID
              $jj++;                                                           //And prepare next array index
              if($val['pListVidId'] != NULL) {                                 //If the playlist contains videos
                $pListArr[$jj] = new PlaylistDownload($this->db, $val['pListId'], 
                                                      $val['pListName'], $val['vidPos'], 
                                                      $val['vidId'], $val['vidTitle'], 
                                                      $val['vidCat'],  $val['vidDescr'], 
                                                      $val['vidDur'], $val['vidFName'], 
                                                      $val['transFName'], $val['vidTags'],
                                                      $val['pListVidId']);
              } else if($val['pListVidId'] == NULL) {                          //Else if the playlist is empty
                $pListArr[$jj] = new PlaylistDownload($this->db, $val['pListId'], 
                                                      $val['pListName'], "", "", "", "", 
                                                      "", "", "", "", "", "");
              }

            }
          }
        }
      }
    }
  }

  public function getPlaylistInfo($vidList = array(), $action) {               //Handles input from playlistEdit.php
    if($this->isLoggedIn()) {
      if(!empty($vidList)) {
        if($action == "update") {                                              //If updating existing playlists
          $this->playlist = array();                                           //Using $jj to separate each different video in each playlist in the local playlist array.
          $ii = $jj = $kk = $mm = 0;                                           //Using $ii to separate each different playlist in the local playlist array. 
          $tempArr = array();                                                  //Using $mm to separate each different unique database playlist Id.    
          
          foreach($vidList as $value) {                                        
            $tempArr[$kk] = explode('-', $value);                              //[$kk][0] = Local array playlist index, [$kk][1] = local array video index, [$kk][2] = database unique playlist Id 
            if($tempArr[$kk][2] == $mm) {                                      //If unique database playlistId == $mm..
              $this->playlist[$ii][++$jj][1] = $tempArr[$kk][1];               //..then we're in the same playlist, the same $ii index.
            } else if($tempArr[$kk][2] > $mm) {                                //Else if unique database playlistId > $mm, then we have a new playlist..
              $mm = $tempArr[$kk][2];                                          //..set $mm to this unique value..
              $jj = 0;                                                         //..reset the $jj video counter..
              $this->playlist[++$ii][$jj][1] = $tempArr[$kk][2];               //..increment the playlist index $ii, use $jj = 0 to store the database unique id
              $this->playlist[$ii][++$jj][1] = $tempArr[$kk][1];               //..increment the video index $jj and store the video position
            }
            $kk++;                                                             //Next temporary element
          }                                                                   
                                                                                      
          $sql = "SELECT playlistVideoId AS id                                 
                  FROM playlistvideo                                           
                  WHERE playlistId = :playlistId AND videoPos = :oldPos";      
          $playlistId = $oldPos = "";                                          
          $stmt = $this->db->prepare($sql);                                    //Prepare fetching the unique playlistVideoId from db
          $stmt->bindParam(':playlistId', $playlistId);                        
          $stmt->bindParam(':oldPos', $oldPos);                                
          $numPlaylists = count($this->playlist);                              
                                                                               
          for($ii = 1; $ii <= $numPlaylists; $ii++){                           //For all playlists
            $numVideos = count($this->playlist[$ii]) - 1;                      //Minus 1 because of the [0] index storing the db unique value
            $playlistId = $this->playlist[$ii][0][1];                          
            for($jj = 1; $jj <= $numVideos; $jj++) {                           //For all videos in each playlist
              $oldPos = $this->playlist[$ii][$jj][1];                          //Save the old video position
              $stmt->execute();                                                
              $row = $stmt->fetch(PDO::FETCH_ASSOC);                           //Fetch the unique playlistVideoId belonging to the video in the old position
              $this->playlist[$ii][$jj][0] = $row['id'];                      
            }
          }
          
        } else if($action == "new") {                                          //If creating a new playlist
          $this->playlist = $this->testInput($vidList);                        //Treating playlist as a string in this case
        }
      }
    } 
  }
  
  public function uploadVideoPlaylists($action) {                              //Uploads an updated or a newly created playlist.
    if($this->isLoggedIn()) {                                                         
      if($action == "update") {
       $newVidPos = $playlistId = $sql = "";
        for($listNum = 1; $listNum <= count($this->playlist); $listNum++)  {                      //For all playlists in the updated playlist arrays
          $playlistId = $this->playlist[$listNum][0][1];                                          //Extracting the unique playlist ID from the database
          for($newVidPos = 1; $newVidPos < count($this->playlist[$listNum]); $newVidPos++) {      //for all videos in each playlist array. (Not <= because of index [0]'s value)
             $oldVidPos = $this->playlist[$listNum][$newVidPos][1];                               //Array index 1->N is representing video position 1-N in each playlist                                                                 
             $uniqueId  = $this->playlist[$listNum][$newVidPos][0];                               //Save the unique playlistVideoId   
            if($newVidPos > $oldVidPos) {                                                         //If current video has been moved further down in the position list                                                           
               $sql = "UPDATE playlistvideo                                                    
                       SET videoPos =  
                       IF(videoPos = :oldPos, :newPos, videoPos -1)                        
                       WHERE playlistId = :playlistId AND playlistVideoId = :uniqueId";   
                       /* IF(condition: true, false) */
               $stmt = $this->db->prepare($sql);
               $stmt->execute(array('oldPos' => $oldVidPos, 'newPos' => $newVidPos,
                                    'playlistId' => $playlistId, 'uniqueId' => $uniqueId));
             } else if($newVidPos < $oldVidPos) {                                                 //If current video has been moved further up in the position list
               $sql = "UPDATE playlistvideo 
                       SET videoPos = 
                       IF(videoPos = :oldPos, :newPos, videoPos +1) 
                       WHERE playlistId = :playlistId AND playlistVideoId = :uniqueId";
               $stmt = $this->db->prepare($sql);
               $stmt->execute(array('oldPos' => $oldVidPos, 'newPos' => $newVidPos,
                                    'playlistId' => $playlistId, 'uniqueId' => $uniqueId));
             }
          }
        }
      } else if($action == "new") {                                                               //The playlist variable is a simple string in this case
        $sql  = "INSERT INTO playlist (uploaderId, name)
                 VALUES (?,?)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array($this->userId, $this->playlist));
      }
    }
  }
 
  public function getUsername() {                                              //Returns the user's first name
    return $this->firstName;
  }
  
  public function deleteVideoFromPlaylist($id) {                               //Deletes a video from a playlist, and changes video positions accordingly 
    if($this->isLoggedIn()) {
      $sql = "SELECT playlistId, videoPos 
              FROM playlistvideo 
              WHERE playlistVideoId = :id";
      $stmt = $this->db->prepare($sql);                                       
      $stmt->execute(array(':id' =>$id));
      $temp = $stmt->fetch(PDO::FETCH_ASSOC);
      $plid = $temp['playlistId'];                                             //Fetch the playlist
      $vidPos = $temp['videoPos'];                                             //Fetch the original video position
      
      $sql = "SELECT MAX(videoPos) AS maxPos 
              FROM playlistvideo 
              WHERE playlistId = :plid";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array(':plid' => $plid));
      $temp = $stmt->fetch(PDO::FETCH_ASSOC);
      $maxPos = $temp['maxPos'];                                               //Fetch the last position
       
      $sql  = "DELETE FROM playlistvideo WHERE playlistVideoId = :id";            
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array('id' => $id));                                      //Delete video from original video position
      
      
      $ii = 0;
      $sql = "UPDATE playlistvideo 
              SET videoPos = 
              IF(videoPos > 0, videoPos - 1, 0)
              WHERE playlistId = :plid AND videoPos = :ii";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam(':ii', $ii);                                            //Binding parameters to execute multiple queries
      $stmt->bindParam(':plid', $plid);
      for($ii = $vidPos + 1; $ii <= $maxPos; $ii++) {                          //For every video after deleted video, increment position
        $stmt->execute();
      }
    } 
  }
 
  public function deleteUserPlaylist($plid) {                                  //Deletes a user playlist and all of its content.
    if($this->isLoggedIn()) {
      $sql  = "DELETE FROM playlist 
               WHERE uploaderId = :userId 
               AND playlistId = :playlistId";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array(':userId' => $this->userId, ':playlistId' => $plid));
    }
  }
 
  public function addVideoToPlaylist($vidPlist) {                              //Adds a video to a playlist from videoBrowse.php
    if($this->isLoggedIn()) {
      $tempArr = explode('-', $vidPlist);                                      //Separates database-unique "playlistId-videoId" numbers
      $vidId = $tempArr[1];
      $listId = $tempArr[0];
     
      $sql= "SELECT MAX(videoPos) AS position
                     FROM playlistvideo
                     WHERE playlistId = :playlistId";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array('playlistId' => $listId));
      $lastPos = $stmt->fetch(PDO::FETCH_ASSOC);                               //Get current max position in specified playlist from database.
      
      if(strlen($lastPos['position']) == 0) {                                  //If empty playlist, start at 1
        $lastPos = 1;
      } else {
        $lastPos = $lastPos['position'] + 1;                                   //If non-empty playlist, add 1 to last position.
      }
      $sql = "INSERT INTO playlistvideo (playlistId, videoId, videoPos)
              VALUES (?,?,?)";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array($listId, $vidId, $lastPos));
    }
  }
 
}
?>
