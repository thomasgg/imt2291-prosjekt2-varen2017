<?php 
class FileDownload {
  protected $fileName, $uploaderId, $uploaderName;                             //Current file's data                                                   
  protected $db;
  
  protected function __construct($fName, $uploader, $db) {
    $this->fileName = $fName;
    $this->uploaderId = $uploader;
    $this->db = $db;
    $this->setUploaderName();
  }
  
  protected function setUploaderName() {
    $sql = "SELECT firstName FROM user
            WHERE userId = :uploaderId";
    $stmt = $this->db->prepare($sql);
    $stmt->execute(array(':uploaderId' => $this->uploaderId));
    if($row = $stmt->fetch()) {
      $this->uploaderName = $row['firstName'];
    }
  }
}
?>