<?php 
class Admin extends Teacher {
  
  public function __construct($db) {
    Teacher::__construct($db);
  }
  
  public function deleteUser($username) {                                      //Deletes a user, for administrators only
    if($this->isLoggedIn()) {
      $sql = "DELETE FROM user WHERE email = :email";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array(':email' => $username));
    }
  }
  
  protected function addUserToDb() {                                           //Adds current user object to user db
    $sql  = "INSERT INTO user (firstName, lastName, email, password)
             VALUES (?,?,?,?)";
    $stmt = $this->db->prepare($sql);
    $usr   = array($this->firstName, $this->lastName,
                  $this->email, $this->pwdHash);
    $stmt->execute($usr);
  }
  
  public function registerUser($fname, $lname, $email, $pwd) {                 //Registers user if not exist
    if($this->isLoggedIn()) {
      $tempMail = $this->testInput($email);
      $sql  = "SELECT email FROM user WHERE email = :email";
      $stmt = $this->db->prepare($sql);
      $stmt->execute(array(':email' => $tempMail));
      if($row = $stmt->fetch()) {
          echo "<br />User already exists.";
      } else {
          $this->firstName = $this->testInput($fname);
          $this->lastName  = $this->testInput($lname);
          $this->email     = $this->testInput($email);
          $this->pwdHash   = password_hash($pwd, PASSWORD_BCRYPT);
          $this->isAdmin   = FALSE;
          $this->isTeacher = FALSE;
          $this->isStudent = TRUE;
          $this->addUserToDb();
      }
    }
  }
  
}
?>