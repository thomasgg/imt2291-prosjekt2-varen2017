<?php
require_once 'classFiledownload.php';

class VideoDownload extends FileDownload {
  private $title, $category, $description,
          $keys, $duration, $transcript,
          $tags, $videoId;

  public function __construct($fName, $uploader, $db, $title, $cat, 
                              $descr, $dur, $transfName, $tags, $id) {
    FileDownload::__construct($fName, $uploader, $db);
    $this->title      = $title;
    $this->category   = $cat;
    $this->descr      = $descr;
    $this->duration   = $dur;
    $this->transcript = $transfName;
    $this->tags       = $tags;
    $this->videoId    = $id;
  }

  public function getVideoId() {
    return $this->videoId;
  }
  
  public function getVideoFileName() {
   return $this->fileName;
  }

  public function getVideoTitle() {
    return $this->title;
  }

  public function getVideoCategory() {
    return $this->subject;
  }

  public function getVideoDescription() {
    return $this->descr;
  }

  public function getVideoDuration() {
    return $this->duration;
  }

  public function getVideoPlaylist() {
   return $this->plid;
  }

  public function getVideoTranscipt() {
    return $this->transcript;
  }

  public function getVideoThumbnail() {
    return substr($this->fileName, 0, -3) . "png";
  }
  
  public function getVideoWatchfile() {
    return substr($this->fileName, 0, -4) . "_watch.txt";
  }
  
  public function getVideoTags() {
    return $this->tags;
  }
  
  public function getVideoPlaylistPos() {
    return $this->playlistPos;
  }
  
  public function getVideoUploaderName() {
    return $this->uploaderName;
  }
  
}


?>
