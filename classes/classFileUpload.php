<?php 
class Fileupload {
  private $db;                                                                 //pdo db-object
  private $fileTempLoc, $usrId, $usrFileName, $dbFileName,                     //Strings: File information, defined by system
          $thumbnailDbName, $fileDest;
  private $vidDuration, $imgHeight, $imgWidth, $fileSize,                      //Strings: File information, defined by file 
          $fileType, $vidFrameBase64;                                          
  private $fileInfo;                                                           //Array:   File information, defined by user
  private $errFlag, $vidFlag, $imgFlag, $txtFlag, $trackSubsFlag,
          $tracksFlag, $trackChapsFlag, $thumbnailFlag, $debugFlag=true;       //Bools
  
  
  public function __construct($db, $file, $usrId) {
    $this->resetFileData();                                                    //Initializes all private variables empty
    if($file['error'] === 0) {
      $this->db = $db;
      $this->usrId = $usrId;      
      $this->fileType = $this->getFileType($file);                             //Get filetype & set file type flag
      if($this->vidFlag) {                                                      
        $this->setVideo($file);                                                //Get video file information
      } else if($this->txtFlag) {
        $this->setText($file);                                                 //Get text file information
      } else if($this->imgFlag) {
        $this->setImage($file);                                                //Get image file information
      } else if($this->tracksFlag) {
        $this->setTracks($file); 
      }
    } else if($file['error'] !== 0 && $this->debugFlag === true) {             //Primarly for debugging
      $this->getFileError($file['error']);
      $this->errFlag = true;      
    }
  }
  
  protected function getFileError($err) {                                      //Errors primarly for debugging
    switch($err) {
      case 1: echo "<br />The uploaded file exceeds the 
                    <strong>upload_max_filesize</strong> directive in <i>php.ini</i>!"; break;
      case 2: echo "<br />The uploaded file exceeds the <strong>MAX_FILE_SIZE</strong> 
                    directive that was specified in the HTML form.";          break;
      case 3: echo "<br />The uploaded file was only partially uploaded.";    break;
      case 4: echo "<br />No file chosen.";                                   break;
      case 6: echo "<br />Missing a temporary folder.";                       break;
      case 7: echo "<br />Failed to write file to disk.";                     break;
      case 8: echo "<br />A PHP extension stopped the file upload. 
                    Examine with phpinfo().";                                 break;
    }
  }
  
  protected function resetFileData() {                                         //Reset all file data
    $this->fileType = $this->usrFileName = $this->dbFileName =
           $this->fileDest = $this->fileTempLoc = 
           $this->vidDuration = "";
    $this->fileSize = $this->imgHeight = $this->imgWidth = 0;
    $this->usrId = -1;
    $this->errFlag = $this->vidFlag = $this->imgFlag = 
           $this->txtFlag = $this->trackSubsFlag = 
           $this->trackChapsFlag = false;
    $this->fileInfo = array();
  }
  
  protected function getFileType($file) {                                      //Extracts filetype & sets fileFlag
    $mime = $file['type'];
    $subType = "badfile";
    if(strpos($mime, "video") !== false) {                                     //If video 
      $subType = substr($mime, strlen("video") + 1);                           
    } else if(strpos($mime, "text") !== false) {                               //if text
      $subType = substr($mime, strlen("text") + 1);                            
    } else if(strpos($mime, "image") !== false) {                              //if image
      $subType = substr($mime, strlen("image") + 1);                           
    } else if(strpos($mime, "application") !== false) {                        //vtt file temporary workaround
      $subType = substr($mime, strlen("application") + 1); 
    }
     /* Allowed similar file types falls through to same destination */
    switch($subType) {                            
      case "mp4"    : $this->vidFlag    = true; return $subType;               //Returns accepted video type
      case "octet-stream":;                                                    //TEMPORARY; vtt mimes are sent like this (security issue)
      case "vtt"    : $this->tracksFlag = true; return $subType;                  
      case "plain"  : $this->txtFlag    = true; return $subType;                 
    
  //  case "bmp"    :  
  //  case "gif"    :  
  //  case "jpeg"   :  
  //  case "png"    : $this->imgFlag = true; return $subType;                  //Returns accepted image type
      case "badfile": 
      default: 
        $this->errFlag = true; 
        return $subType;                                                       //Flags error, returns forbidden type
    }
    
  }
  
  protected function setImage($file) {                                         //Saves image file information
    /* Image-functions not implemented */
    $maxSize = 22416000;                                                      
    $minSize = 50;    
    $imgInfo = getimagesize($file['tmp_name']);
    $this->imgWidth  = $imgInfo[0];
    $this->imgHeight = $imgInfo[1];
  }
  
  protected function setText($file) {                                          //Saves text file information
    /* Text-functions not fully implemented */
    $maxSize = 21665;                                                       
    $minSize = 1;
    if($file['size'] >= $maxSize || $file['size'] <= $minSize) {
      $this->errFlag = true;
    } else {                                                                   //else save video file info
      $this->fileTempLoc = $file['tmp_name'];                                  //Saves temporary filepath
      $this->fileSize    = $file['size'];                                      //File size in bytes
      $this->usrFileName = $file['name'];                                      //File name specified by uploader
      $this->dbFileName  =
           uniqid('usr' . $this->usrId . 'usr', true) . '.' . $this->fileType; //Creating an uniqe storage file name, containing user id
      $this->fileDest    = "../uploads/text/" . $this->dbFileName;             //WARNING: filepath must be changed dependant on from where the class is included
    }
  }
  
  protected function setVideo($file) {                                         //Saves video file information
    $maxSize = 2324000000;                                                     //2.324GB
    $minSize = 100000;                                                         //100kb
    if($file['size'] >= $maxSize || $file['size'] <= $minSize) {               //If video too big or too small
      $this->errFlag     = true;                                               //..set error flag
    } else {                                                                   //else save video file info
      $this->fileTempLoc = $file['tmp_name'];                                  //Saves temporary filepath
      $this->fileSize    = $file['size'];                                      //File size in bytes
      $this->usrFileName = $file['name'];                                      //File name specified by uploader
      $this->dbFileName  =
           uniqid('usr' . $this->usrId . 'usr', true) . '.' . $this->fileType; //Creating an uniqe storage file name, containing user id
      $this->fileDest    = "../uploads/videos/" . $this->dbFileName;           //WARNING: filepath must be changed dependant on from where the class is included
    }
  }
  
  protected function setTracks($file) {
    $maxSize = 21665;                                                       
    $minSize = 1;
    if($file['size'] >= $maxSize || $file['size'] <= $minSize) {
      $this->errFlag = true;
    } else {                                                                   //else save tracks file info
      $this->fileTempLoc = $file['tmp_name'];                                  //Saves temporary filepath
      $this->fileSize    = $file['size'];                                      //File size in bytes
      $this->usrFileName = $file['name'];                                      //File name specified by uploader
      if(strpos(file_get_contents($this->fileTempLoc), "chapter") !== false) { //If tracks file contains string "chapter".
        $this->trackChapsFlag = true;
      } else if(strpos(file_get_contents($this->fileTempLoc), "subtitle") !== false) {
        $this->trackSubsFlag = true;
      } else {
        $this->errFlag = true;
      }
    }
  }
  
  protected function testInput($input) {                                       //Checks for bad input, prevents XSS
    $input = trim($input);                                                     //Removes extra spaces
    $input = stripslashes($input);                                             //Removes slashes
    $input = htmlspecialchars($input);                                         //All characters interpreted as HTML entities
    return $input;
  }
  
  protected function saveVideoDuration() {                                     //Saves video duration as a string, adds a slight delay
    if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {                           //If Windows OS
        $this->vidDuration = shell_exec("powershell -ExecutionPolicy Bypass -Command ../scripts/ps/videoDuration.ps1 -folderPath '../uploads/videos' -fileName '$this->dbFileName'");  //WARNING: filepath must be changed dependant on from where the class is included
     } else {                                                                  //Else NIX..
        echo "Windows OS not detected. Video length not stored.";
      }  
  }
  
  public function addTracksToVideo($vidFileName) {                             //Handles video track files connected to a specific video.
    if($this->trackChapsFlag) {
      $this->dbFileName = substr($vidFileName, 0, -4) . "_chaps_en.vtt";       //Only working with english subtitles for now.
    } else if($this->trackSubsFlag) {    /* Create language en = english etc function. Create parameters to this function with language from user */
      $this->dbFileName = substr($vidFileName, 0, -4) . "_subs_en.vtt";      
    }
    $this->fileDest   = "../uploads/transcripts/" . $this->dbFileName;         //WARNING: filepath must be changed dependant on from where the class is included
  } 
  
  public function saveWatchlistToDisk() {                                      //When video is uploaded, a watcFile is automatically created
    $watchFileDbName = substr($this->dbFileName, 0, -4) . "_watch.txt";        //Watchlist txt file uses the video name file with a watch suffix.
    $filePath = "../uploads/text/" . $watchFileDbName;                         //WARNING: Filepath must be changed dependant on where its called from
    $watchFile = fopen($filePath, 'w');                                        //Creates empty txt file
    fclose($watchFile);
  }
  
  public function saveThumbnailToDisk($thumbnail) {                            //Saves decoded base64 image code as an image to disk
    $this->vidFrameBase64 = array();
    $this->vidFrameBase64 = explode(',', $thumbnail);                         
    $this->thumbnailDbName = substr($this->dbFileName, 0, -3) . "png";         //Replaces mp4 with png. Same filename
    $filePath = "../uploads/images/" . $this->thumbnailDbName;                 //WARNING: filepath must be changed dependant on from where the class is included
    $thumbnailFile = fopen($filePath, 'w');                                    //Opens filepath/name.png for writing
    fwrite($thumbnailFile, base64_decode($this->vidFrameBase64[1]));
    fclose($thumbnailFile);
  }
 
  public function saveFileInfoToDb() {                                         //Adds a file entry in the file table connected to the user
    $sql = "";
    if($this->vidFlag) {                                                       //If video file..
      $sql  = "INSERT INTO video(uploaderId, title, category, description, 
                                 tags, duration, vidFileName, transFileName)
               VALUES(?,?,?,?,?,?,?,?)";
    } else if($this->imgFlag) {                                                //If image file..
      //under construction
      $sql = "INSERT INTO image"; 
    } else if($this->txtFlag) {                                                //If text file..
      //under construction
      #$sql = "INSERT INTO text";
    }
    $stmt = $this->db->prepare($sql);
    $stmt->execute($this->fileInfo);
  }
  
  public function saveFileToDisk() {                                           //Saves, if possible, the file to file server
    if(move_uploaded_file($this->fileTempLoc, $this->fileDest)) {
    } else {
      $this->errFlag = true;
    }
  }
 
  public function isFileOk() {                                                 //Returns TRUE if the uploaded file is OK
    return (($this->errFlag) ? FALSE : TRUE);      
  }
  
  public function getFlagType() {                                              //Returns file type (video/image/text) as string
    if($this->vidFlag) {
      return "video";
    } else if($this->imgFlag) {
      return "image";
    } else if($this->txtFlag) {
      return "text";
    } else if($this->trackSubsFlag) {
      return "subtitles";
    } else if($this->trackChapsFlag) {
      return "chapters";
    } else if($this->thumbnailFlag) {
      return "thumbnail";     //For future implementation of custom thumbnail uploads
    }
  } 
    
  public function getFileInfo($fileParams = array()) {                         //Saves user defined file data                   
    if($this->vidFlag) {      
      $this->saveVideoDuration();                                              //Length can only be obtained by Powershell after file is saved to disk
      $this->fileInfo[0] = $this->usrId;  //Might take this out of vidFlag
      $this->fileInfo[1] = $this->testInput($fileParams['title']);
      $this->fileInfo[2] = $this->testInput($fileParams['category']);
      $this->fileInfo[3] = $this->testInput($fileParams['description']);
      $this->fileInfo[4] = strtolower($this->testInput($fileParams['tags']));
      $this->fileInfo[5] = $this->vidDuration;
      $this->fileInfo[6] = $this->dbFileName;
      $this->fileInfo[7] = ""; 
      //checkDuplicate() ??? if title&uploader exists, assume duplicate
    } else if($this->imgFlag) {
      //under construction
    } else if($this->txtFlag) {
      //under construction
    }
  }
      
}

?>