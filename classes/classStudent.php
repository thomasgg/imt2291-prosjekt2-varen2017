<?php 
class Student extends User {
  
  public function __construct($db) {
    User::__construct($db);
  }
  
  protected function addWatchfileEntry($vidFileName, $sec) {                   //Adds student email to watchfile for a specific video    
   $watchfilePath = "../uploads/text/" . substr($vidFileName, 0, -4) . "_watch.txt";//WARNING: Filepath must be changed dependant on where its called from
   if($this->isFileAvailable($watchfilePath)) {                    	           //If file exists, (empty allowed)
     
     if($this->getWatchfileEntry($vidFileName) != 0) {                         //If watchtime is not 0, then the user is already logged in the file.
       $userEntryArray = array();                                              //Create a temp array to hold all watchfile contents
       $offset = strpos(file_get_contents($watchfilePath), $this->email);      //Find where to start the over-write.
       
       $userEntryArray = explode('-', file_get_contents($watchfilePath, NULL, NULL, $offset)); //The user entry exists in array index [0]       
       $tempEntry = explode('_', $userEntryArray[0]);                          //Separating email & watchTime into to index [0] and [1], respectively
       $tempEntry[1] = $sec;                                                   //Writing new watchtime to entry.
       $userEntryArray[0] = $tempEntry[0] . "_" . $tempEntry[1];               //Formating back to ex: "email@email_10"
       $numEntries = count($userEntryArray) - 1; 															 //Explode will treat the last "-" as a new empty index, we'll subtract this.
       
       $watchfile = fopen($watchfilePath, 'r+');                               //Open file in read/write mode.
       fseek($watchfile, $offset);                                             //Set file pointer to user entry offset

       for($ii = 0; $ii < $numEntries; ++$ii) {                                //For all entries
         $userEntryArray[$ii] = $userEntryArray[$ii] . "-";                    //Append back the "-" to the entry
         fwrite($watchfile, $userEntryArray[$ii]);                             //Over-write watchfile with array contents from user-entry and out.
       }
       fclose($watchfile);
       
     } else {                                                                  //Else just append new user to file.
      echo "TEST: APPENDING TO FILE";
      $watchfile = fopen($watchfilePath, 'a');                                 //Open file in append mode
      fwrite($watchfile, $this->email . "_" . $sec . "-");                     //Write "<email>_<x>s-" to file where x is seconds watched video
      fclose($watchfile);
     }
   }
  }
  
  protected function getWatchfileEntry($vidFileName) {                         //Returns user's listed watch time from watchfile.
    $watchfilePath = "../uploads/text/" . substr($vidFileName, 0, -4) . "_watch.txt";//WARNING: Filepath must be changed dependant on where its called from
    if($this->isFileAvailable($watchfilePath)) {   
      $watchfile = fopen($watchfilePath, 'r');
      
      if(strpos(file_get_contents($watchfilePath), $this->email) !== false) {  //If watchfile contains user's email
        $userEntryArray = array();
        $offset = strpos(file_get_contents($watchfilePath), $this->email);     //The offset is the byte containing this user's entry-start
        $userEntryArray = explode('-', file_get_contents($watchfilePath, NULL, NULL, $offset)); //The user entry exists in array index [0]
       
        return (explode('_', $userEntryArray[0])[1]);                          //Returns user's seconds watched number (after '_')
      }
    }
    return 0;                                                                  //Returns 0 if user entry is not found in file or file does not exist
  }
  
  public function registerWatchTime($startTime = array(), $endTime = array(), $vidFileName) {
    /*
      Have not checked what a 1min + video outputs. 60+ seconds or change format??
      Handle format transform in this funk if so
    */
    if($this->isLoggedIn()) {
      $length = count($startTime);
      $total = 0;
      for($ii = 0; $ii < $length; ++$ii) {
        $total += ($endTime[$ii] - $startTime[$ii]);                           //Summing up all the change in times.
      }
      echo $total;

      if($total > $this->getWatchfileEntry($vidFileName)) {                    //If watchfile entry has less seconds registered
        $this->addWatchfileEntry($vidFileName, ceil($total));                  //Overwrite with new watchfile entry. Using ceil to avoid 0.xxx == 0, and thus getWatchFileEntry == 0.
      }
    }
  }
  
}
?>